/*
 * Unbuffered AJAX output (wrapper to $.ajax)
 * Copyright (c) 2009 Matt Saladna <matt@apisnetworks.com>
 * Dual licensed under the MIT and GPL licenses.
 *
 * Unbuffered AJAX executes a request, while simultaneously
 * polling an output file for changes.  Designed as a cross-browser
 * solution for accessing the XMLHttpRequest object during
 * an interactive readyState (3).
 *
 * @author Matt Saladna matt@apisnetworks.com
 */


var _aub_state = {xhr: undefined, tail_xhr: undefined, timer: undefined, offset: -1};

/*
 * @param  {object} [options] parameters inherited from $.ajax
 *
 * In addition to the parameters inherited from $.ajax,
 * the following options are recognized:
 *
 * output:     (Object:$('#output_status'))
 *             Element to receive command output
 * onUpdate:   (Function:null)
 *             Callback each time output is updated
 *
 * @link http://docs.jquery.com/Ajax/jQuery.ajax
 *
 */
(function($) {
   $.ajax_unbuffered = function (options) {

		// time between requests, backs off on slow drains
		var latency = 300;
		var reqCompleted = false;
		var ie = apnscp.browser.type == "msie";
		var o = {
			output: $('#output_status'),
			dataType: 'json',
			onUpdate: function() {o.output.get(0).scrollTop += 100000},
			beforeSend: null,
			complete: null,
			success: null,
			file: null,
			indicator: $('span.ui-ajax-indicator')
		};
		var callbacks = {
			beforeSend: function(xhr){
				if (options.beforeSend) {
					if (options.beforeSend.call(this, xhr) === false)
						return;
				}
				o.indicator.removeClass('ui-ajax-success ui-ajax-error').addClass('ui-ajax-loading').
					children('.ui-ajax-status').html('Running').fadeIn();
				stop();
				_aub_state.xhr = xhr;
				reqCompleted = false;
				setTimeout(function(){
					tail_output();
				}, 500);

			},
			complete: function(status){
				if (options.complete) {
					if (options.complete.call(this) === false)
						return;
				}
				reqCompleted = true;
				// one last call to flush remaining buffer
				tail_output();
				clearTimeout(_aub_state.timer);
				_aub_state.offset = 0;
				$('.ui-ajax-indicator').removeClass('ui-ajax-loading');
			},
			error: function(xhr, status, err) {
				stop();
				o.indicator.removeClass('ui-ajax-loading').addClass('ui-ajax-error');
				return true;
			},
			success: function(data, status){
				if (options.success) {
					if (options.success.apply(this, [data, status]) === false)
						return;
				}
				var indicatorClass = 'ui-ajax-success', msg = 'Done', mode = 'hide';
				/**
				 * backwards compatibility with older ajax command defintions
				 */
				if (!data || data && data.success != undefined && !data.success) {
					indicatorClass = 'ui-ajax-error';
					msg = 'Failed';
					if (status.errors) {
						msg += ': ' + status.errors[0];
					}
					mode = 'show';
				}
				o.indicator.removeClass('ui-ajax-loading').addClass(indicatorClass).
					children('.ui-ajax-status').html(msg).effect('highlight', {
					mode: mode
				}, 3500);

			}
		};

		o = $.extend({},o,options, callbacks);
		if (!o.file) {
		   throw new Error("No file specified to tail");
		}
		var stop = function() {
			clearTimeout(_aub_state.timer);
			_aub_state.offset = 0;
			reqCompleted = true;
			if (_aub_state.xhr) {
				_aub_state.xhr.abort();
			}
			clearTimeout(_aub_state.timer);
		}

		var tail_output = function () {
			_aub_state.tail_xhr = $.ajax({
				url: '/ajax_engine?engine=tail',
				data: {fn: 'tail', file: o.file, offset: _aub_state.offset},
				type: 'GET',
				dataType: 'json',
				/**
				 * Prevent lingering request for lock when
				 * the command exits too quickly
				 */
				beforeSend: function(xhr) {
					if (reqCompleted && _aub_state.offset == -1)
						return false;
					_aub_state.tail_xhr = xhr;
				},
				success: function(data) {
					// to-do, add error handling
					// backoff 100 ms if no data
					if (!data) {
						_aub_state.offset = -1;
						return false;
					}
					if (data.length < 1 && latency < 2000) {
						latency += 100;
					} else {
						if (ie) data.data = $('<span />').text(data.data);
						o.output.append(data.data);
						o.onUpdate(data.length);
						if (latency > 100 && latency < 750)
							latency -= 50;
						else if (latency > 750)
							latency = 300;
					}
					if (data.eof && reqCompleted) {
						_aub_state.offset = -1;
						return clearTimeout(_aub_state.timer);
					} else if (data.eof || reqCompleted) return;

					_aub_state.offset = data.offset;
					_aub_state.timer = setTimeout(tail_output, latency);
				}
			});
		};
		return $.ajax(o);
    };
	$.fn.extend($.ajax_unbuffered);
})(jQuery);
