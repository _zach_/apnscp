@php
	$parent_stat = $Page->statParent();
@endphp
<div class="row mb-3">
	<div class="col-12">

		@if (!empty($parent_stat['can_write']))
		<div class="btn-group">
			<button type="button" name="upload" class="btn btn-secondary dropdown-toggle mt-1" id="newMenuBtn"
			        aria-haspopup="true" data-toggle="dropdown" aria-expanded="false" aria-controls="newMenu">
				<i class="fa fa-plus"></i>
				New
			</button>
			<div class="dropdown-menu" aria-labelledby="newMenuBtn" id="newMenu">
				<a href="#" name="Create_File" id="Create_File" data-modal-content="#new-file-modal" data-toggle="modal"
				   data-target="#modal" class="dropdown-item">
					<i class="fa fa-file-o"></i>
					File
				</a>
				<a href="#" class="dropdown-item" data-target="#modal" data-toggle="modal"
				   data-modal-content="#new-folder-modal" id="Create_Directory">
					<i class="fa fa-folder-o"></i>
					Folder
				</a>
				<a href="" class="dropdown-item" data-toggle="modal" data-modal-content="#new-link-modal"
				   data-modal-content="#new-link-modal" id="Create_Link">
					<i class="fa fa-link"></i>
					Link
				</a>
			</div>
		</div>

		<button type="button" data-modal-target="#download-remote-modal"
		        class="btn-secondary btn ui-action-download-remote mt-1"
		        id="DownloadRemote" value="Download File and Extract" name="Download_Remote">
			Download Remote
		</button>

		<button type="button" name="upload" class="btn btn-secondary mt-1 dropdown-toggle"
		        data-target="#uploadContainer" data-toggle="collapse"
		        aria-expanded="{{ $Page->isUpload() ? 'true' : 'false' }}" aria-controls="uploadContainer">
			<i class="fa fa-upload"></i>
			Show Upload
		</button>


		@endif

		<div class="btn-group">
			<button type="submit" name="Add_Clipboard" id="clipboard"
		        class="btn btn-secondary mt-1">
					<i class="fa fa-plus"></i>
					Add to Clipboard
			</button>
			<button class="btn btn-secondary dropdown-toggle-split dropdown-toggle mt-1" name="Clipboard" id="showClipboard" data-target="#clipboardMenu"
			        data-toggle="collapse" aria-expanded="false" aria-controls="clipboardMenu" type="button">
				<span class="sr-only">Toggle Clipboard</span>
			</button>
		</div>
		<button name="Download_Folder" class="ui-action-download btn btn-secondary mt-1" type="submit">
			Download Directory
		</button>

		@if (!empty($parent_stat['can_write']))
			<button name="Delete" class="ui-action-delete btn-secondary ui-action-delete btn warn mt-1" type="submit"
			        onClick="return confirm('Are you sure you want to remove the selected files?');">
				Delete Selected Files
			</button>
		@endif
	</div>

	<div class="collapse @if ($Page->isUpload()) show @endif" id="uploadContainer">
		<div class="m-3 p-3">
			<div class="row">
				<div class="col-12" id="progressBox"></div>
			</div>
			<div class="row">
				<h6 class="col-12">Upload file to {{ $Page->getCurrentPath() }}</h6>
			</div>
			<div class="row mt-3">
				<div class="col-sm-6 col-12 py-1" style="">
					<label class="custom-file w-100">
						<input type="file" id="upload-file" class="custom-file-input">
						<span class="custom-file-control">
                            <i class="fa fa-upload"></i>
                        </span>
					</label>&nbsp;
				</div>
				<div class="hidden-sm-down col-12 col-sm-6 py-1 pr-1" id="dragdrop">
					or <i class="fa fa-download"></i> drag &amp; drop
				</div>
				<div class="col-12">
					<label class="custom-checkbox custom-control"
					       for="upload_overwrite">
						<input type="checkbox" class="custom-control-input" name="upload_overwrite"
						       id="upload_overwrite"/>
						<span class="custom-control-indicator"></span>
						overwrite destination file on conflict
					</label>
				</div>
				<div class="col-12">
					<div id="container-upload-list"></div>
				</div>

				<div class="col-12">
					<p class="note">
						Max upload size is <?=ini_get('upload_max_filesize')?> &mdash;
						use <a class="ui-action ui-action-label ui-action-switch-app" href="/apps/webdisk">WebDisk</a>
						or
						<a class="ui-action ui-action-label ui-action-kb"
						   href="{{ MISC_KB_BASE }}{{ $vars['ftp_kb'] }}">FTP</a> for larger files.
					</p>
					<p class="note">
						A compressed file may be uploaded, then extracted within the File Manager.
					</p>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="form-group my-3 row collapse" id="clipboardMenu">
	<h4 class="col-12 d-flex align-items-center">
		<label class="custom-checkbox custom-control my-1 align-items-center d-flex" for="clipboard-selectall">
			<input id="clipboard-selectall" type="checkbox" name="" class="custom-control-input" />
			<span class="custom-control-indicator"></span>
			<i class="ui-action-clipboard"></i>
			Clipboard Contents
		</label>
	</h4>


	@foreach ($Page->getClipboardContents() as $clipboardItem)
		<div class="col-12 clipboard entry mb-1">
			<label class="custom-control custom-checkbox mb-0 clipboard-item">
				<input id="clipboard-selectall" type="checkbox" name="Clipboard_Items[]"
				       class="custom-control-input" value="{{ $clipboardItem }}"/>
				<span class="custom-control-indicator"></span>
				<span class="clipboard-item">{{ $clipboardItem }}</span>
			</label>
		</div>
	@endforeach

	<div class="col-12">

		<label class="mb-0 d-block">
			With checked clipboard items:
		</label>
		<button type="submit" class="btn-outline-secondary btn my-3 ml-0 mr-2" name="Clipboard_Download"
		        value="Download"><i class="fa fa-download"></i> Download
		</button>
		<button type="submit" class="btn-outline-secondary btn my-3 mr-2" name="Clipboard_Copy" value="Copy"><i
					class="fa fa-files-o"></i> Copy
		</button>
		<button type="submit" class="btn-outline-secondary btn my-3 mr-2" name="Clipboard_Move" value="Move"><i
					class="ui-action ui-action-transfer"></i> Move
		</button>
		<button type="submit" class="btn-outline-secondary btn my-3 mr-2" name="Clipboard_Symlink" value="Symlink"><i
					class="fa fa-link"></i> Link
		</button>
		<button type="submit" class="btn btn-outline-secondary warn my-3" name="Clipboard_Clear" value="Clear"><i
					class="fa fa-times"></i> Clear
		</button>&nbsp;
	</div>
</div>


<div class="modal-dialogs hide">
	<div class="download-modal text-left">
		<label class="form-control-label">Remote URL</label>
		<div class="input-group">
			<input class="form-control" placeholder="https://wordpress.org/latest.zip" type="text" size="50"
			       name="remote_url" id="remote_url" value=""/>
		</div>
		<label class="form-control-label d-block">
			<input type="checkbox" class="checkbox" name="extract" value="1" checked="CHECKED"/>
			Extract After Downloading
		</label>
		<button type="submit" name="Download_Remote" value="1" class="btn btn-primary">
			<i class="fa fa-download"></i>
			Download
		</button>
		<input type="hidden" name="cwd" value="{{ $Page->getCurrentPath() }}"/>
	</div>

	<div class="create-link-modal text-left" id="new-link-modal">
		<div class="row">
			<div class="col-12">
				<label class="d-block form-control-label">
					Link Name
					<input id="symlink_name" type="text" placeholder="mainwebsite_html" class="form-control"
					       name="symlink_name"/>
				</label>
				<label class="d-block form-control-label">
					Target
					<div class="input-group">
						<input type="text" class="form-control" name="symlink_target"
						       value="<?=$Page->getCurrentPath()?>" id="browse_target"/>
						<button id="browse_path" class="float-right btn btn-secondary" name="browse_path"><i
									class="fa fa-folder-open"></i>Browse
						</button>
					</div>
				</label>

				<button type="submit" value="Create Directory" data-post="file_symlink"
				        class="mt-3 btn-primary btn ui-action-label ui-action-folder-new" name="Create_Symlink">
					Create Symlink
				</button>
			</div>

		</div>
	</div>

	<div class="create-link-modal text-left" id="new-folder-modal">
		<div class="row">
			<div class="col-12">
				<label class="d-block form-control-label">
					Folder Name
					<input id="folder_name" type="text" placeholder="My Folder" class="form-control"
					       name="Directory_Name"/>
				</label>

				<button type="submit" value="Create Directory" data-post="file_create_directory"
				        class="btn-primary btn ui-action-label ui-action-folder-new" name="Create">
					Create Folder
				</button>
			</div>
		</div>
	</div>

	<div class="create-link-modal text-left" id="new-file-modal">
		<div class="row">
			<div class="col-12">
				<label class="d-block form-control-label">
					File Name
					<input id="file_name" type="text" placeholder="My File.txt" class="form-control" name="File_Name"/>
				</label>

				<button type="submit" value="Create Directory" data-post="file_touch"
				        class="my-3 btn-primary btn ui-action-label ui-action-file-new" name="Create_File">
					Create File
				</button>

			</div>
		</div>
	</div>

</div>
