
var bounding;
$(document).ready(function() {
	bounding = document.getElementById('terminal');
	bounding.onload = resizeIframe;

	function resizeIframe() {
		bounding.style.height = bounding.contentWindow.document.body.scrollHeight + 'px';
		setTimeout(resizeIframe, 250);
	}
});

$(window).on('load', function() {
	var pollSelf = function () {
		if (bounding && $('#connectPassword:visible', bounding.contentDocument).length > 0) {
			$('#connectPassword', bounding.contentDocument).val(RSPAMD_PASSWORD);
			setTimeout(function () {
				$('#connectButton', bounding.contentDocument).click();
			}, 500);

			return true;
		}
		setTimeout(pollSelf, 50);
	};
	pollSelf();
});