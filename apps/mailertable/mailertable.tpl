<?php
?>
<!-- main object data goes here... -->

<div class="row">
	<h3 class="col-12">Add Hostname</h3>
</div>
<form method="POST" action="/apps/mailertable.php" class="form-inline" id="entry-form">

	<div class="flex-column  mb-2 mr-sm-2 mb-sm-0">
		<label for="subdomain" class="justify-content-start">
			Subdomain
		</label>
		<select name="subdomain" class="form-control custom-select" id="subdomain">
			<option value="">&lt;None&gt;</option>
			<?php foreach ($Page->get_subdomains() as $subdomain) {
			printf('
			<option value="%s">%s</option>
			', $subdomain, $subdomain);
			} ?>
		</select>
	</div>

	<div class="flex-column  mb-2 mr-sm-2 mb-sm-0">
		<label for="domain" class="justify-content-start">
			Domain
		</label>
		<select name="domain" class="form-control custom-select" id="domain">
			<?php
                    foreach($Page->get_aliases() as $domain) {
			printf('
			<option value="%s">%s</option>
			', $domain, $domain);
			} ?>
		</select>
	</div>

	<input type="hidden" name="add_table"/>
	<button type="submit" name="add_table" value="Add Entry" id="add_table"
	        class="align-self-end form-control primary btn btn-primary add  mb-2 mb-sm-0">Add Entry
	</button>
</form>
<div class="row">
	<div class="col-12">
		<p class="note mt-1 hidden-sm-down">Once a hostname is added, you will be able to create email addresses (<span
					id="example">e.g. &lt;sample&gt;@<span id="ex-subdomain"></span><span id="ex-domain"></span></span>)
			using this domain via
			<a href="/apps/mailboxroutes" class="ui-action ui-action-label ui-action-switch-app">Manage Mailboxes</a>
		</p>
	</div>
</div>


<div class="row">
	<h3 class="col-12">Active Hostnames</h3>
	<div class="col-12">
		<p class="hidden-sm-down note">
			Any hostname present in this table authorizes the server to handle email for this domain. Any hostname
			present below will <a class="ui-action ui-action-kb ui-action-label"
			                      href="<?=MISC_KB_BASE?>/email/mail-sent-hosted-domain-not-arrive-third-party-mx-records/">ignore</a>
			third-party MX settings and instead deliver locally to the specified user on this server.
		</p>
	</div>
</div>

<div class="row" id="routes">
	<?php
        $entries = $Page->get_mailer_table_entries();
	if (count($entries) < 1):
	?>
	<div class="col-md-4 ui-infobox">
		This server is not authorized to handle mail for any domain on this account.
		Enable authorization for hostnames above.
	</div>
	<?php else:
            foreach ($entries as $domain):
                echo '<div class="col-xl-2 col-md-4 col-lg-3 col-sm-6 col-12 mt-1">',
					'<a class="ui-action btn btn-secondary btn-block warn ui-action-label ui-action-delete" href="#"
					    rel="'.base64_encode($domain).'" title="delete">',
						'<i class="fa fa-times"></i> ',
						$domain,
					'</a></div>';
endforeach;
?>
<?php endif; ?>
</div>