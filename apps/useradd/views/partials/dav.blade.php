<hr/>
<fieldset class="form-group my-3 row">
	<h5 class="col-12 form-group-label">
		<i class="fa fa-upload"></i>
		WebDAV
		<span class="small" data-toggle="tooltip"
		      title="Includes Web Disk (Files > Web Disk). Unlike FTP, users cannot be jailed."><i
					class="fa fa-sticky-note-o"></i> What's this?</span>
	</h5>
	<div class="col-sm-6">
		<label class="pl-0 form-inline custom-checkbox custom-control align-items-center d-flex">
			<input type="checkbox" class="custom-control-input" name="dav_enable" id="dav_enable"
			       value="1" {{ $Page->get_option('dav_enable', 'checkbox') }} />
			<span class="custom-control-indicator"></span>
			Enable
		</label>
	</div>
</fieldset>