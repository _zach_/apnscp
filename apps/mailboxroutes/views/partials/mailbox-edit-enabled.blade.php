<div class="btn-group">
	<a class="ui-action ui-action-edit btn btn-secondary ui-action-label" title="edit" href="#"
	   data-toggle="modal" data-target="#editModal">
		Edit
	</a>
	<button class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
	        aria-expanded="false"></button>
	<div class="dropdown-menu dropdown-menu-right">
		@if ($account['type'] === \Email_Module::MAILBOX_USER)
			<a class="dropdown-item ui-action-label ui-action-user"
			   href="/apps/useredit?user={{ $account['user'] }}">
				User Settings
			</a>
			@if ($Page->is_vacation($account['destination']))
				<a href="{{ HTML_Kit::new_page_url_params(null, ['mode' => $Page->getMode(), 'user' => $account['destination'], 'vacation' => false]) }}"
		            data-user="{{ $account['user'] }}" data-mode="disable"
		            class="ui-action ui-action-vacation-disable ui-action-label warn dropdown-item">
					Disable Vacation
				</a>
			@else
				<a href="{{  HTML_Kit::new_page_url_params(null, ['mode' => $Page->getMode(), 'user' => $account['destination'], 'vacation' => true]) }}"
		            data-user="{{  $account['user'] }}" data-mode="enable"
		            class="ui-action ui-action-vacation-enable ui-action-label warn  dropdown-item">
					Enable Vacation
				</a>
			@endif
			<div class="dropdown-divider"></div>
		@endif
		<a class="ui-action ui-action-disable dropdown-item ui-action-label"
		   href="#"
		   onclick="return confirm('Are you sure you want to disable the mailbox \'{{ $account['user'] . "@" . $account['domain'] }}\'?') &&
				   apnscp.assign_url('{{ HTML_Kit::new_page_url_params(null, ['mode' => $Page->getMode(), 'disable' => $addrHash]) }}');">
			Disable
		</a>

		<a class="dropdown-item warn ui-action-label" href="#" title="delete" href="#"
		   onclick="return confirm('Are you sure you want to delete \'{{ $account['user'] . " @" . $account['domain'] }}\'?')
				   &&
				   apnscp.assign_url('{{ HTML_Kit::new_page_url_params(null, ['mode' => $Page->getMode(), 'd' => $addrHash]) }}');">
			<i class="ui-action ui-action-delete"></i>
			Delete
		</a>
	</div>
</div>