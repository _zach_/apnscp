@php
$bulk = array('username' => array(), 'domain' => array(), 'destination' => array(), 'type' => array('V', 'A'));
@endphp

@foreach ($accounts as $account)
	@php
		if (strtoupper($account['type']) == 'V' &&
			preg_match('!^/home/([^/]+)/Mail[/.]*(.*)$!', $account['mailbox'], $mailbox_simple)) {
			$account['mailbox'] = $mailbox_simple[1] . ($mailbox_simple[2] ? ' -&gt;
				' . 'INBOX\\' . str_replace('.', '\\', $mailbox_simple[2]) : '');
		} else {
			$account['mailbox'] = join(", ", explode('/,\s*/', $account['mailbox']));
		}

		if (strtoupper($account['type']) == 'V')
			$account['type'] = \Email_Module::MAILBOX_USER;
		else if (strtoupper($account['type']) == 'A')
			$account['type'] = \Email_Module::MAILBOX_FORWARD;
		else if (strtoupper($account['type']) == 'P')
			$account['type'] = 'S';

@endphp

<tr class="entry @if ($account['type'] === \Email_Module::MAILBOX_USER && $Page->is_vacation($account['destination'])) vacation @endif">
	<td class="text-center">
		<input type="checkbox" name="address[]"
		       value="{{ $account['user'] . '@' . $account['domain'] }}"/>
	</td>
	<td id="email_{{ $loop->index }}" class="@if (!$account['enabled']) text-disabled @endif user">
		@if ($account['type'] === \Email_Module::MAILBOX_USER)
			<i class="ui-action-vacation" data-toggle="tooltip"
		        data-title="User `{{ $account['destination'] }}' is on vacation"></i>
		@endif
		<span class="user">
			{{ $account['user'] }}
		</span>
	</td>
	<td id="domain_{{ $loop->index }}" class="@if (!$account['enabled']) text-disabled @endif domain">
		<span class="domain">
			{{ $account['domain'] }}
		</span>
	</td>
	<td data-mailbox-type="{{ $account['type'] }}"
	    class="@if (!$account['enabled']) text-disabled @endif mailbox">
		@php
			$type = $account['type'];
			if ($type === \Email_Module::MAILBOX_USER) {
				$css = 'ui-action-email-local';
			} else if ($type === \Email_Module::MAILBOX_FORWARD) {
				$css = 'ui-action-email-forward';
			} else {
				$css = 'ui-action-email-special';
			}
		@endphp
		<span class="mailbox ui-action ui-action-label {{ $css }}">
            {{ implode(", ", preg_split("/[,;\s]+/", $account['mailbox'])) }}
        </span>
	</td>
	<td class="" align="center">
		@include(
			"partials.mailbox-edit-${listMode}",
			[
				'account'  => $account,
				'addrHash' => base64_encode($account['user'] . '|' . $account['domain'] . '|' . $account['type'])
			]
		)
	</td>
</tr>
@endforeach