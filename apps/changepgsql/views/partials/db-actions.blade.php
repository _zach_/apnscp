<button type="submit"
        class="btn btn-primary ui-action ui-action-label ui-action-compress hidden-sm-down"
        value="Vacuum" name="Vacuum[{{ $dbname }}]">Vacuum
</button>
<button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown"
        aria-haspopup="true" aria-expanded="false">
	<span class="sr-only">Toggle Dropdown</span>
</button>
<div class="dropdown-menu">
	<button type="submit"
	        class="btn ui-action ui-action-label ui-action-compress btn-block hidden-md-up secondary"
	        value="Vacuum" name="Vacuum[{{ $dbname }}]">Vacuum
	</button>
	<button name="export[{{ $dbname }}]" value=""
	        class="ui-action-download btn-block dropdown-item ui-action-label ui-action">
		Export
	</button>
	<button name="import[{{ $dbname }}]" data-db="{{ $dbname }}" value=""
	        class="ui-action-restore btn-block dropdown-item ui-action-label ui-action">
		Restore from Backup
	</button>
	<button name="snapshot[{{ $dbname }}]" data-db="{{ $dbname }}" value=""
	        class="ui-action-snapshot btn-block dropdown-item ui-action-label ui-action">
		Snapshot
	</button>
	<div class="dropdown-divider"></div>
	<button type="submit"
	        class="ui-action-ban btn-block dropdown-item ui-action-label dropdown-item  warn ui-action"
	        name="empty[{{ $dbname }}]"
	        onClick="return confirm('Are you sure you want to empty all tables in the database {{ $dbname }}?');">
		<i class="fa fa-trash-o"></i> Empty Tables
	</button>

	<button type="submit"
	        class="ui-action ui-action-label btn-block ui-action-delete dropdown-item warn"
	        value="{{ $dbname }}" name="drop"
	        onClick="return confirm('Are you sure you want to drop the database {{ $dbname }}?');">
		Delete Database
	</button>