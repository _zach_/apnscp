<tr class="entry">
	<td class="">
		<div class="input-group">
			<div class="btn-group">
				@include('partials.db-actions')
				</div>
			</div>
		</div>
	</td>
	<td>
		{{ $dbname }}
	</td>
	<td>
		<select name="newuser" class="custom-select" data-db="{{ $dbname }}" data-owner="{{ $dbowner }}" data-toggle="changedb" data-language="pgsql">
			@foreach (array_keys($users) as $user)
				<option value="{{ $user }}" @if	($user === $dbowner) SELECTED @endif
					>{{ $user }}</option>
			@endforeach
		</select>
	</td>
	<td>
		{{ $Page->getDBSize($dbname) }}
	</td>
</tr>