@component('theme::partials.app.modal')
	@slot('title')
		Snapshots &ndash; {{ $app->getHostname() }}
	@endslot
	@slot('id')
		importModal
	@endslot
	<div>
		<h5>Restore from backup</h5>
		<div class="">
			<div class="form-group">
				<ul class="backup-restore list-unstyled mb-0" id="backup-list">
				</ul>
				<p class="note">
					No snapshots configured for {{ $app->getHostname() }}.
				</p>
				<div class="help-block with-errors text-danger"></div>
			</div>
			<hr/>
			<fieldset class="form-group">
				<label class="custom-control custom-checkbox mb-0">
					<input name="enable" type="checkbox" class="custom-control-input" value="1" id="confirm"
					       required/>
					<span class="custom-control-indicator"></span>
					<span>
						I understand that restoring a snapshot will delete all
						data presently in <b class="">{{ $app->getAppRoot() }}</b>. This cannot be undone.
					</span>
				</label>
				<div class="help-block with-errors text-danger"></div>
			</fieldset>
		</div>
	</div>
	@slot('buttons')
		<button type="submit" name="git-restore" class="btn btn-primary restore ajax-wait">
			<i class="fa fa-pulse d-none"></i>
			Restore
		</button>
		<p class="ajax-response"></p>
	@endslot
@endcomponent