<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace apps\majordomo;

	use Page_Container;

	include_once(INCLUDE_PATH . '/lib/configuration_driver.php');

	class Page extends Page_Container
	{


		public function __construct()
		{
			parent::__construct();
		}

		public function on_postback($params)
		{
			if (isset($params['delete_list']) && $params['delete_list']) {
				$this->majordomo_delete_mailing_list($params['delete_list']);
			} else if (isset($params['create'])) {
				$admin = \Util_Conf::get_svc_config('siteinfo', 'admin_user');
				if ($params['new_list'] == $admin) {
					return error("List cannot be named after admin user `%s'", $admin);
				}
				$this->majordomo_create_mailing_list($params['new_list'], $params['list_password'],
					$params['admin_email'], $params['domain']);
			} else if (isset($params['save_users'])) {
				$this->majordomo_set_mailing_list_users($params['edit_users'], $params['email_list']);
			} else if (isset($params['save_config'])) {
				// merge options from stock setting with those submitted
				// if a value is disabled, it is omitted from postback and must be filled in
				unset($params['save_config']);
				$list = $_GET['edit_config'];
				$origoptions = $this->majordomo_load_configuration_options($list);
				$newopts = $params;
				foreach (array_keys($origoptions) as $o) {
					if (!isset($newopts[$o])) {
						$newopts[$o] = false;
					}
				}

				$compiledopts = $this->majordomo_change_configuration_options($newopts);

				$this->majordomo_save_configuration_options($list, $compiledopts);
			}

		}

		public function get_list_address()
		{
			$domain = $this->majordomo_get_domain_from_list_name($_GET['edit_users']);

			return $_GET['edit_users'] . '@' . $domain;
		}

		public function parse_config($mList)
		{
			$conf = $this->majordomo_load_configuration_options($mList);
			$this->bind($conf);

			return $conf;
		}

		public function get_email()
		{

			return $this->common_get_admin_email();
		}

		public function get_lists()
		{
			return $this->majordomo_list_mailing_lists();
		}

		public function get_users($mList)
		{
			return $this->majordomo_get_mailing_list_users($mList);
		}

		public function get_domains()
		{
			$domains = $this->email_list_virtual_transports();
			\Util_Conf::sort_domains($domains);
			$key = null;
			if (false !== ($key = array_search(\Util_Conf::login_domain(), $domains))) {
				unset($domains[$key]);
				array_unshift($domains, \Util_Conf::login_domain());
			}

			return $domains;
		}

		public function render_html($mOptName, array $mData)
		{
			$data = $label = '';
			if (($mData['type'] & bool) != 0) {
				$data .= '<label class="form-control-label"><input type="checkbox" class="checkbox-inline" name="' . $mOptName . '" ' . ((isset($mData['value']) && $mData['value'] != 'no' && $mData['value'] != 'n') ? sprintf("CHECKED") : '') . '/> enable</label>';
			} else if (($mData['type'] & enum) != 0) {
				$data .= '<select class="form-control custom-select" name="' . $mOptName . '">';
				$i = 0;
				foreach ($mData['values'] as $k => $v) {
					if (is_array($v)) {
						$data .= $v['label'] . " ";
						$data .= $this->renderHTML($k, $v['values']);
					} else {
						$data .= '<option ' . ((isset($mData['value']) && ($mData['value'] == $v)) ? 'SELECTED ' : '') . 'value="' . ($v) . '">' . (!is_int($k) ? $k : $v) . '</option>';
					}
					$i++;
				}
				$data .= '</select>';

			} else if (($mData['type'] & text) != 0) {
				if (!isset($mData['value'])) {
					$mData['value'] = '';
				}
				$data .= '<textarea class="form-control" name="' . $mOptName . '">' . $mData['value'] . '</textarea>';
			} else if (($mData['type'] & set) != 0) {
				$optStr = '<select class="form-control custom-select" multiple="MULTIPLE" name="' . $mOptName . '" SIZE="3">';
				foreach ($mData['values'] as $optName => $optValue) {
					$optStr .= $optValue . ' <option ' . (($mData['default'] == $optValue) ? sprintf('SELECTED') : '') . 'value="' . $optValue . '">' . $optValue . '</option>';
				}
				$data .= $optStr . '</select>';
			} else if (($mData['type'] & string | integer) != 0) {
				$data .= '<input class="form-control" type="text" name="' . $mOptName . '" value="' . (isset($mData['value']) ? ($mData['value']) : '') . '"/>';
			}

			return $data;
		}

		public function convert_type_to_string($mType)
		{
			switch ($mType) {
				case enum:
					return 'enum';
				case string:
					return 'string';
				case int:
					return 'integer';
				case text:
					return 'text';
				case bool:
					return 'bool';
			}
		}

	}

?>
