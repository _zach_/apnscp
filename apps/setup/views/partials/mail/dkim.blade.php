<tr>
	<th>
		<span class="ui-action-copy" data-clipboard-text="{{ $auth->dkimSelector() }}._domainkey"
		      title="Copy to Clipboard" id="clipboardCopyDkim"
		      data-toggle="tooltip">
			{{_("DKIM selector")}} ({{ _("DNS record") }})
		</span>

	</th>
	<td>
		<code>{{ $auth->dkimSelector() }}</code>
	</td>
</tr>
<tr>
	<th>
		<span class="ui-action-copy" data-clipboard-text="{{ $auth->dkimKey() }}"
		      title="Copy to Clipboard" id="clipboardCopyDkimKey"
		      data-toggle="tooltip">
			{{_("DKIM key")}}
		</span>
	</th>
	<td>
		<code class="d-block" aria-describedby="clipboardCopyDkimKey" class="mr-2">
			{!! wordwrap($auth->dkimKey(), 80, "<br />", true) !!}
		</code>
	</td>
</tr>
<tr>
	<th>
		{{_("DKIM DNS record")}}
	</th>
	<td>
		<div class="d-flex flex-row">
			<div class="mr-2">
				<div class="dkim-heading text-uppercase small" >
					<span class="ui-action-copy" data-toggle="tooltip" title="Copy to Clipboard"
					      id="clipboardCopyDkimHostname"
					      data-clipboard-text="{{ $auth->dkimSelector() }}._domainkey.{{ $auth->domain() }}">
						{{ _("Hostname") }}
					</span>
				</div>
				<code class="d-block" aria-describedby="clipboardCopyDkimHostname">
					{{ $auth->dkimSelector() }}._domainkey.{{ $auth->domain() }}
				</code>
			</div>
			<div class="mr-2">
				<div class="dkim-heading text-uppercase small">
					RR
				</div>
				<code class="d-block">
					TXT
				</code>
			</div>
			<div class="mr-2">
				<div class="dkim-heading text-uppercase small">
					<span class="ui-action-copy" data-toggle="tooltip" title="Copy to Clipboard"
					      id="clipboardCopyDkimParameter"
					      data-clipboard-text="{{ $auth->dkimRecord() }}">
						{{ _("Parameter") }}
					</span>
				</div>
				<code class="d-block">
					{!! wordwrap($auth->dkimRecord(), 80, "<br />", true) !!}
				</code>
			</div>
		</div>
	</td>
</tr>