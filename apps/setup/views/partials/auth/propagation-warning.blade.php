<div class="alert alert-warning">
	<h5>Domain pending propagation</h5>
	<p>
		Your domain looks to use third-party nameservers or have
		third-party DNS assigned. We're showing generic configuration
		that will work now, but recommend checking back for server-neutral
		configuration in a couple days once the dust settles!
	</p>

	<p class="font-italic">
		Detected IP {{ $auth->dns_gethostbyname_t($auth->domain(), 2000) }}. Want {{ $auth->dns_get_public_ip() }}.
	</p>
</div>