<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace apps\ssl;

	use Error_Reporter;
	use HTTP_Download;
	use Module\Support\Webapps;
	use Page_Container;
	use ZipArchive;

	class Page extends Page_Container
	{
		protected $certificate;
		private $_cleanup = array();
		private $_leCapable;
		private $_certInfo;

		public function __construct()
		{
			parent::__construct();
			$this->add_css('ssl.css');
			$this->add_javascript('ssl.js');
			$this->init_hostname_map();
		}

		public function _render()
		{
			$this->reloadCertificate();
			if ($this->certificate->isLetsEncrypt()) {
				// LE loaded automatically
				$this->add_javascript('GLOBAL_MODE="LE";', 'internal', false);
			}
			$this->view()->share(['certificate' => $this->certificate]);
		}

		private function reloadCertificate() {
			$ctor = [];
			if ($this->certificateInstalled()) {
				$certinfo = $this->getCertificates()[0];
				$ctor = [$this->getPrivateKey($certinfo['key']), $this->getCertificate($certinfo['crt'])];
			}
			$this->certificate = CertificateResource::instantiateContexted(\Auth::profile(), $ctor);
			if ($ctor && !empty($certinfo['chain'])) {
				$this->certificate->setChain($this->getCertificate($certinfo['chain']));
			}
			return true;
		}

		private function init_hostname_map()
		{
			$hostnames = $this->get_hostnames();

			$js = 'var host_map = {subdomain: {';
			$maps = array();
			foreach ($hostnames['subdomain'] as $subdomain => $domains) {
				$maps[] = "'" . $subdomain . "'" . ': [' .
					trim("'" . join("','", $domains), ",") . "']";
			}
			$js .= implode(',', $maps) . '}, domain: { ';
			$maps = array();
			foreach ($hostnames['domain'] as $domain => $subdomains) {
				$maps[] = "'" . $domain . "'" . ': [' .
					trim("'" . implode("','", $subdomains), ",") . "']";
			}
			$js .= implode(',', $maps) . '}};';
			$this->add_javascript($js, 'internal', false);
		}

		public function get_hostnames()
		{
			$domains = Webapps::getAllPartitionedHostnames($this->getAuthContext(), true, true);
			$xtrafields = [];
			foreach ($this->email_provisioning_records(\UCard::init()->getDomain()) as $record) {
				if (!$record->matches('rr', 'A') && !$record->matches('rr', 'AAAA')) {
					continue;
				}
				if (isset($xtrafields[$record['name']])) {
					continue;
				}
				$xtrafields[$record['name']] = 1;
				// add dns check?
				$domains['subdomain'][$record['name']] = $domains['subdomain'][''];
			}
			array_walk($domains['domain'], static function (&$v) use ($xtrafields) {
				$v[] = '*';
				$v += append_config(array_keys($xtrafields));
				asort($v);
			});
			$domains['subdomain']['*'] = $domains['subdomain'][''];
			return $domains;
		}

		public function get_aliases()
		{
			$aliases = \Util_Conf::all_domains();
			\Util_Conf::sort_domains($aliases);

			return $aliases;
		}

		public function certificateInstalled()
		{
			return $this->ssl_cert_exists();
		}

		public function getCertificates()
		{
			if (!isset($this->_certInfo)) {
				$this->_certInfo = $this->ssl_get_certificates();
			}

			return $this->_certInfo;
		}

		public function getPrivateKey($key)
		{
			return $this->ssl_get_private_key($key);
		}

		public function getCertificate($certificate)
		{
			return $this->ssl_get_certificate($certificate);
		}

		public static function load_states($cc)
		{
			return \HTML_Kit::states($cc);
		}

		public function __destruct()
		{
			if ($this->_cleanup) {
				foreach ($this->_cleanup as $file) {
					if (file_exists($file)) {
						unlink($file);
					}
				}
			}
		}

		public function on_postback($params)
		{
			if (isset($params['files'])) {
				var_dump($params);
				die();
			}

			if (isset($params['download'])) {
				/**
				 * @invoker Download Key + Certificate
				 */
				$this->reloadCertificate();
				$this->_prepareCertDownload($params['download']);
			} else if (isset($params['generate'])) {
				// generate either "self" or "csr"
				if ($params['generate'] == "lecert") {
					if (!isset($params['agree'])) {
						return error("you must agree to the Let's Encrypt Subscriber's Agreement");
					}

					return $this->_installLetsEncrypt($params);
				}
				if (!$this->ssl_key_exists()) {
					$key = $this->ssl_generate_privatekey();
					if (!$key) {
						return false;
					}
					info("generated a new private key");
				} else {
					$key = $this->ssl_get_private_key();
					$info = $this->ssl_privkey_info($key);
					info("reusing private key");
				}
				$host = $params['hostname'][0] ?? trim(implode(".", array($params['subdomain'], $params['domain'])), ".");
				$params['cn'] = $host;
				$csr = $this->_generateCSR($key, $params);
				if (!$csr) {
					return false;
				}
				if ($params['generate'] == "self") {
					// generate self-signed, install cert too
					$crt = $this->ssl_sign_certificate($csr, $key, \Ssl_Module::X509_DAYS);

					return $this->_installCertificate($key, $crt);
				} else {
					// download a CSR request
					$files = array();
					$files[] = array(
						'name' => $this->_escapeFilename($host) . '.csr',
						'data' => $csr
					);
					$files[] = array(
						'name' => $this->_escapeFilename($host) . '.key',
						'data' => $key
					);

					return $this->_prepareCsrDownload($host, $files);
				}
			} else if (isset($params['regenerate'])) {
				/**
				 * @invoker Generate Request (CSR)
				 */
				$certconfig = $this->_getCertificateConfigurationFromCertificate($params['generate']);

				if (!$certconfig) {
					return error("cannot find certificate configuration for `%s'", $params['generate']);
				}
				$certdata = $this->getCertificate($certconfig['crt']);
				$certinfo = $this->getCertificateInfo($certdata);
				$hostname = $this->certificate->getHostnames();
				$hostname = array_pop($hostname);
				if (!isset($certinfo['subject'])) {
					Error_Reporter::report(var_export($certinfo, true));

					return error("unable to generate CSR: " .
						"cannot determine subject field from certificate");
				}
				$subject = $certinfo['subject'];
				$email = null;
				if (isset($subject['Email'])) {
					$email = $subject['Email'];
				} else if (isset($subject['emailAddress'])) {
					$email = $subject['emailAddress'];
				}

				if (!isset($subject['CN'])) {
					$subject['CN'] = $hostname;
				}
				$subject['E'] = $email;

				$key = $this->getPrivateKey($certconfig['key']);

				$csr = $this->_generateCSR($key, $subject);

				if (!$csr) {
					Error_Reporter::report(var_export($certinfo, true));

					return error("cannot determine CSR data from certificate! file ticket for help");
				}
				$this->_prepareCsrDownload($hostname, $csr);
			} else if (isset($params['replace'])) {
				$this->_replaceCertificate($params['replace']);
			} else if (isset($params['delete'])) {
				$this->_deleteCertificate($params['delete']);
			} else if (isset($params['import'])) {
				$this->_importCertificate();
			}
		}

		/**
		 * Prepare a certificate for download
		 *
		 * @invoker download
		 * @param $certname
		 * @return bool
		 */
		private function _prepareCertDownload($certname)
		{
			$certconfig = $this->_getCertificateConfigurationFromCertificate($certname);

			if (!$certconfig) {
				return error("unknown certificate `%s'", $certname);
			}
			// use this later to get alternative hostnames
			$certdata = $this->getCertificate($certconfig['crt']);
			$files = array(
				array(
					'name'    => $certconfig['crt'],
					'data'    => $certdata,
					'comment' => 'SSL certificate'
				),
				array(
					'name'    => $certconfig['key'],
					'data'    => $this->getPrivateKey($certconfig['key']),
					'comment' => 'Private key'
				)
			);
			if (isset($certconfig['chain'])) {
				$files[] = array(
					'name'    => $certconfig['chain'],
					'data'    => $this->getCertificate($certconfig['chain']),
					'comment' => 'Certificate chain'
				);
			}
			$mapping = "File listing: " . "\r\n" .
				"Certificate file: " . $certconfig['crt'] . "\r\n" .
				"Private key: " . $certconfig['key'] . "\r\n";

			if (isset($certconfig['chain'])) {
				$mapping .= "Intermediate/Chain Certificate: " . $certconfig['chain'] . "\r\n";
			}

			$domains = $this->certificate->getHostnames();
			$mapping .= "Hostname validity:" . "\r\n";
			foreach ($domains as $d) {
				$mapping .= "\t" . $d . "\r\n";
			}

			$mapping .= "\r\nRequest generated on " . date('r');
			$files[] = array(
				'name'    => 'README.txt',
				'data'    => $mapping,
				'comment' => 'Read me first!'
			);
			$this->_prepareZipDownload($files, $domains[0]);
		}

		private function _getCertificateConfigurationFromCertificate($crt)
		{
			$certs = $this->getCertificates();

			foreach ($certs as $c) {
				if ($c['crt'] == $crt) {
					return $c;
				}
			}

			return null;
		}

		/**
		 * Add files to zip
		 *
		 * @param array  $files files
		 * @param string $name  filename
		 */
		private function _prepareZipDownload($files, $name)
		{
			$zip = new ZipArchive();
			$oldmask = umask();
			umask(077);
			$tmpfile = tempnam(TEMP_DIR, 'ssl');
			umask($oldmask);
			$zip->open($tmpfile, ZipArchive::CREATE);
			$this->_cleanup[] = $tmpfile;

			foreach ($files as $file) {
				$zip->addFromString($file['name'], $file['data']);
				if (isset($file['comment'])) {
					$zip->setCommentName($file['name'], $file['comment']);
				}
			}

			$zip->close();
			$opts = array('file' => $tmpfile);
			$output = new HTTP_Download($opts);

			$domainpretty = $this->_escapeFilename($name);
			$dlname = 'ssl-' . $domainpretty . '.zip';
			$output->setContentDisposition(HTTP_DOWNLOAD_ATTACHMENT, $dlname);
			$output->send();
			unlink($tmpfile);
			exit();
		}

		private function _escapeFilename($name)
		{
			return str_replace(array("*", "."), array("WC", "-"), $name);
		}

		private function _installLetsEncrypt($params)
		{
			if (isset($params['hostname']) && is_array($params['hostname'])) {
				$cn = $params['hostname'];
			} else {
				$cn = array();
				$params = array(
					array(
						'subdomain' => $params['subdomain'],
						'domain'    => $params['domain']
					)
				);

				foreach ($params as $host) {
					$hostname = ltrim($host['subdomain'] . '.' . $host['domain'], ".");
					$cn[] = $hostname;
				}
			}

			if (empty($cn) && $this->certificateInstalled()) {
				// no hostnames, remove SSL
				$cert = $this->getCertificates();
				if (!$cert) {
					return error("No certificate installed");
				}
				foreach ($cert as $c) {
					$this->_deleteCertificate($c['crt']);
				}

				return info("SSL disabled on account");
			}

			$verify = !empty($params['verifyip']);
			\Preferences::set(\Letsencrypt_Module::SKIP_IP_PREFERENCE, $verify);

			return $this->letsencrypt_request($cn, $verify);
		}

		private function _deleteCertificate($crt)
		{
			$config = $this->_getCertificateConfigurationFromCertificate($crt);
			if (!$config) {
				return error("unknown certificate `%s'", $crt);
			}
			if (!isset($config['chain'])) {
				$config['chain'] = null;
			}
			$this->ssl_delete(
				$config['key'],
				$config['crt'],
				$config['chain']
			);

		}

		private function _generateCSR($key, array $data)
		{
			$keys = array('CN', 'C', 'ST', 'L', 'O', 'OU', 'E');
			$newdata = array();
			$data = array_change_key_case($data, CASE_UPPER);
			foreach ($keys as $k) {
				if (!empty($data[$k])) {
					$newdata[$k] = $data[$k];
				} else {
					$newdata[$k] = null;
				}
			}
			$csr = call_user_func([$this, 'ssl_generate_csr'],
				$key,
				$newdata['CN'],
				$newdata['C'],
				$newdata['ST'],
				$newdata['L'],
				$newdata['O'],
				$newdata['OU'],
				$newdata['E'],
				$data['HOSTNAME'] ?? []
			);

			return $csr;
		}

		private function _installCertificate($key, $crt, $chain = null)
		{
			if (!$this->ssl_verify_x509_key($crt, $key)) {
				return error("certificate does not match key");
			}

			if ($chain && !$this->ssl_verify_certificate_chain($crt, $chain)) {
				return error("certificate chain provided is insufficient/incorrect");
			}

			return $this->ssl_install($key, $crt, $chain);

		}

		private function _prepareCsrDownload($name, $data)
		{
			if (!is_array($data)) {
				$data = array(
					array(
						'data' => $data,
						'name' => $name . '.csr'
					)
				);
			} else {
				// key bundled
				foreach ($data as $d) {
					if (!isset($d['data'])) {
						return error("csr download missing data");
					}
					if (!isset($d['name'])) {
						return error("csr download missing name");
					}
				}
			}

			$this->_prepareZipDownload($data, $name);
		}

		public function getCertificateInfo($cert)
		{
			return $this->ssl_parse_certificate($cert);
		}

		private function _replaceCertificate($crt)
		{
			$config = $this->_getCertificateConfigurationFromCertificate($crt);
			if (!$config) {
				return error("invalid certificate `%s'", $crt);
			}
		}

		private function _importCertificate()
		{
			$files = $_FILES;
			var_dump($files);
		}

		public function getPublicKey($key)
		{
			return $this->ssl_get_public_key($key);
		}

		public function getPublicKeyInfo($key)
		{
			return $this->ssl_get_public_key_info($key);
		}

		public function validateCertificate($certificate, $key)
		{
			return $this->ssl_valid($certificate, $key);
		}

		public function validateKey($key)
		{
			return $this->ssl_verify_key($key);
		}

		public function validateChain($certificate, $chain)
		{
			return $this->ssl_verify_certificate_chain($certificate, $chain);
		}

		public function getSelected($param)
		{
			if (isset($_POST[$param])) {
				return $_POST[$param];
			}

			return null;
		}

		public function get_subdomains()
		{
			$subs = array_map(
				function ($sub) {
					return !($pos = strpos($sub, ".")) ? $sub : substr($sub, 0, $pos);
				},
				array_keys($this->web_list_subdomains())
			);
			$subs[] = 'www';
			$subs[] = '*';

			\Util_Conf::sort_domains($subs);

			return $subs;
		}

		public function useLetsEncrypt()
		{
			if ($this->_leCapable !== null) {
				return $this->_leCapable;
			}

			$this->_leCapable = $this->letsencrypt_permitted();

			return $this->_leCapable;
		}

		public function sslPermitted()
		{
			return $this->ssl_permitted();
		}
	}