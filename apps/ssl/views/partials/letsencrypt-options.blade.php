<fieldset class="form-group" id="letsencrypt-legal">
	<label class="custom-control custom-checkbox d-block">
		<input class="custom-control-input" type="checkbox"
		       @if (\Preferences::get(\Opcenter\Crypto\Letsencrypt\Preferences::VERIFY_IP, LETSENCRYPT_VERIFY_IP)) checked="CHECKED"
		       @endif name="verifyip" id="verifyip"
		       value="1"/>
		<span class="custom-control-indicator"></span>
		Perform <a href="#" class="ui-action-tooltip ui-action-label" data-toggle="popover"
		           data-title="IP Check"
		           data-content="Verify the hostname matches {{ cmd("dns_get_public_ip") }}. Disable if using CloudFlare or another reverse proxy and manually renew with Beacon. IP checks are automatically enabled on certificate renewals. A HTTP reachability test will still be performed consistent with Let's Encrypt's ACME service. See documentation for workaround.">IP
			check</a> on each hostname before authorizing certificate
	</label>

	<label class="custom-control custom-checkbox d-block">
		<input type="checkbox" class="custom-control-input"
		       name="agree" value="1" id="agree"/>
		<span class="custom-control-indicator"></span>
		I agree to the Let's Encrypt
		<a href="https://letsencrypt.org/repository/">Subscriber's Agreement</a>
	</label>
</fieldset>

<input type="hidden" name="le-regen" value="1"/>