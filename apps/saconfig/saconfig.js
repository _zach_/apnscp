var COPY_SUCCESS_MSG = 'Copied successfully';
$(window).on('load', function () {
	Spamassassin.activate('saconfig');
});

var Spamassassin = {
	activate: function (formName) {
		// collapse fixes
		var form = document.forms[formName], lastPop = null;

		$(form).find('[data-toggle="collapse"]').on('click', function (e) {
			var nodeType = $(this).get(0).nodeName.toUpperCase(),
				targetProp = nodeType === "INPUT" ? 'checked' : 'selected';
			e.stopPropagation();
			$(this).parent().trigger('click');
			$($(this).data('target')).collapse($(this).prop(targetProp) ? 'show' : 'hide');
		});

		var clipboard = new Clipboard('[data-clipboard-target]');
		clipboard.on('success', function (e) {
			var originalTitle = $(e.trigger).attr('data-original-title');
			$(e.trigger).attr('data-original-title', COPY_SUCCESS_MSG).tooltip('show').on('hidden.bs.tooltip', function (e) {
				$(e.currentTarget).attr('data-original-title', originalTitle);
			});

			e.clearSelection();
		});

		// popovers
		$(form).on('click', '.popover-trigger', function () {
			var tip = $(this).attr('id'), $el = $(this);
			if (lastPop) {
				lastPop.popover('dispose');
				if (lastPop.get(0) == this) {
					lastPop = null;
					return false;
				}
			}
			$.getJSON($(this).attr('href'), function (data) {
				if (!data.success) {
					return;
				}
				lastPop = $el.popover({
					html: true,
					title: data.return.title,
					content: data.return.body,
					trigger: 'focus'
				}).popover('show');
			});
			return false;
		});

		window.updateOpts = function updateOpts(obj) {
			if (obj.name == "mailbox_type") {
				if (obj.value == "pop3") {
					form.delivery_mailbox[2].disabled = true;
					form.delivery_mailbox[3].disabled = true;
					form.delivery_mailbox[3].disabled = true;
					form.delivery_bias.disabled = true;
					form.delivery_bias_mailbox.disabled = true;
					form.delivery_bias_score.disabled = true;
				} else {
					form.delivery_mailbox[2].disabled = false;
					form.delivery_mailbox[3].disabled = false;
					form.delivery_mailbox[3].disabled = false;
					form.delivery_bias.disabled = false;
					form.delivery_bias_mailbox.disabled = false;
					form.delivery_bias_score.disabled = false;

				}
			} else if (obj.name == "bayesian_filtering") {
				if (!obj.checked) {
					form.bayesian_rules.disabled = true;
					form.bayes_auto_learn.disabled = true;
					form.bayes_ham_activate.disabled = true;
					form.bayes_spam_activate.disabled = true;
					form.bayes_min_ham.disabled = true;
					form.bayes_min_spam.disabled = true;
				} else {
					form.bayesian_rules.disabled = false;
					form.bayes_auto_learn.disabled = false;
					form.bayes_ham_activate.disabled = false;
					form.bayes_spam_activate.disabled = false;
					form.bayes_min_ham.disabled = false;
					form.bayes_min_spam.disabled = false;

					if (!form.bayes_auto_learn.checked) {
						form.bayes_min_ham.disabled = true;
						form.bayes_min_spam.disabled = true;
					} else {
						form.bayes_min_ham.disabled = false;
						form.bayes_min_spam.disabled = false;
					}
				}
			} else if (obj.name == "bayes_auto_learn") {
				if (!obj.checked) {
					form.bayes_min_ham.disabled = true;
					form.bayes_min_spam.disabled = true;
				} else {
					form.bayes_min_ham.disabled = false;
					form.bayes_min_spam.disabled = false;
				}
			}
		}

		function setInitial() {

			form.delivery_mailbox[2].disabled = (form.mailbox_type[1].checked);
			form.delivery_mailbox[3].disabled = (form.mailbox_type[1].checked);
			form.delivery_bias.disabled = (form.mailbox_type[1].checked);
			form.delivery_bias_mailbox.disabled = (form.mailbox_type[1].checked);
			form.delivery_bias_score.disabled = (form.mailbox_type[1].checked);
			if (form.hasOwnProperty("bayes_auto_learn")) {
				form.bayes_auto_learn.disabled = (!form.bayesian_filtering.checked);
				form.bayes_min_ham.disabled = !(form.bayesian_filtering.checked ||
					form.bayes_auto_learn.checked);
				form.bayes_min_spam.disabled = !(form.bayesian_filtering.checked ||
					form.bayes_auto_learn.checked);
				form.bayesian_rules.disabled = (!form.bayesian_filtering.checked);
				form.bayes_ham_activate.disabled = !form.bayesian_filtering.checked;
				form.bayes_spam_activate.disabled = !form.bayesian_filtering.checked;
			}
		}

		setInitial();
	}
};