<div class="row">
	<div class="col-12">
		<h4 class="step">Setup and Installation</h4>
		<p class="lead">All done! How would you like to handle the generated files?</p>
		<ul class="list-unstyled pl-0 ml-0">
			<li>
				<label class="custom-control custom-radio mb-0">
					<input type="radio" data-target="#ftpConfig" data-toggle="collapse"
					       name="output" onChange="updateOpts(this);" class="custom-control-input"
					       value="apply" @if ($output !== 'view') CHECKED @endif />
					<span class="custom-control-indicator"></span>
					<span>
						Save changes
					</span>
				</label>
			</li>
			<li>
				<label class="custom-control custom-radio mb-0">
					<input type="radio" name="output" value="view" class="custom-control-input"
					       onChange="updateOpts(this);" @if ($output === "view") CHECKED @endif />
					<span class="custom-control-indicator"></span>
					<span>
						Display it in my browser; I will copy the files over.
					</span>
				</label>
			</li>
		</ul>
	</div>
</div>