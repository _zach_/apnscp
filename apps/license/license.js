/*
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, January 2019
 */
(function($) {
	var $activationForm, $indicator, dialogWindow;

	$indicator = $('<i></i>');
	$(document).ready(function () {
		$activationForm = $('#activate-license');
		$activationForm.on('submit', function () {
			var key = $('.license-key', this).val();
			dialogWindow.find('.ui-ajax-error-msg').remove();
			dialogWindow.find('.btn-primary').find('.ui-ajax-indicator').remove().end().append($indicator).prop('disabled', true);
			apnscp.cmd('admin_activate_license', [key], null, { useCustomHandlers:true, indicator: $indicator }).done(function (ret) {
				dialogWindow.find('.modal-body').empty().append($('<p>').addClass('ui-ajax-success-msg').
				text("Success! The panel is automatically restarting. Please wait a few seconds before refreshing."));
			}).fail(function (xhr, textStatus, errorThrown) {
				var status = $.parseJSON(xhr.responseText);
				if (status['errors']) {
					var $err = [];
					$err.push($('<p class="ui-ajax-error-msg">').text("Key activation failed"));
					for (var i in status['errors']) {
						$err.push($('<p class="ui-ajax-error-msg">').text(status['errors'][i]));
					}
					dialogWindow.find('.modal-body').append($err);
				}
			}).always(function(obj) {
				var status = typeof obj === 'object' ? 'error' : 'success';
				dialogWindow.find('.btn-primary').prop('disabled', false);
				$indicator.removeClass('ui-ajax-loading').addClass('ui-ajax-' + status);
			});
			return false;
		});

		$('#activateLink').on('click', function () {
			activateModal();
			return false;
		});
	});

	function activateModal() {
		dialogWindow = apnscp.modal($activationForm);
		dialogWindow.modal();
	}
})(jQuery);