<form method="post" data-toggle="validator" action="<?=\HTML_Kit::page_url_params(array('mode' => $Page->getMode()))?>">
	<!--  inactive  -->
	<div id="inactive-container" class="mb-3">
		<div class="alert alert-warning">
			<p>
				<strong>ATTENTION</strong> &mdash; The following domains are unattached to this account.
				<strong>Activate Changes</strong> to bring
				configuration up-to-date with the account. Sites listed below will remain inoperable until then.
			</p>
			<button type="submit" name="synchronize"
			        class="mt-1 ui-action btn btn-primary ui-action-activate ui-action-label" value="Activate Changes">
				Activate Changes
			</button>
		</div>
		<ul id="inactive-domains" class="list-unstyled list-inline list-group mb-1">
			@if (isset($inactive['add']))
				@foreach ($inactive['add'] as $domain)
					<li class='domain-add list-group-item list-group-item-success list-inline-item ui-action ui-action-label ui-action-add'>
						{{ $domain }}
					</li>
				@endforeach
			@endif
			@if (isset($inactive['remove']))
				@foreach ($inactive['remove'] as $domain)
					<li class='domain-remove list-group-item list-group-item-danger list-inline-item ui-action ui-action-label ui-action-delete'>
						{{ $domain }}
					</li>
				@endforeach
			@endif
		</ul>
	</div>
</form>