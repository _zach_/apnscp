@include('partials.switch-mode')

@php
	$inactive = $Page->list_inactive_domains();
	$DEFAULT_FORM_ACTION = \HTML_Kit::page_url_params(array('mode' => $Page->getMode()));
@endphp
@includeWhen($inactive && !empty($inactive['add']) || !empty($inactive['remove']), 'partials.inactive-domains')

@if ($Page->getMode() == 'add')
	@php
		$subdomains = $Page->get_subdomains();
		$users = \Util_Conf::users();
		unset($users[\Util_Conf::get_svc_config('siteinfo', 'admin_user')]);
	@endphp

	@include('partials.add-domain', ['mailTransports' => $Page->list_transports()])
@else
	@include('partials.view-domains')
@endif

<img src="/images/apps/domainmanager/activate-modal.png" alt="" class="png24 preload"/>
