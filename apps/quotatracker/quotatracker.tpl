<?php
    namespace apps\quotatracker;
?>
<!-- main object data goes here... -->
<h3 class="text-center">Usage Graph</h3>
<span id="storage-graph"></span>

<h3 class="text-center">Storage Information</h3>
<table width="100%" class="table" id="storageTable">
	<thead>
	<tr>
		<th class="" width="150">User</th>
		<th class="">Current Usage</th>
		<th class="">Storage Threshold Date</th>
		<th class=" hidden-sm-down">Daily Change</th>
	</tr>
	</thead>
	<?php

        foreach($Page->getStorageData() as $user => $eqinfo) {
	$model = $eqinfo['eq'];
	$start = $eqinfo['start'];
	$quota_info = $Page->getCurrentUsage($user);
	$expectedTS = -1;
	if ($model['b'] > 0)
	{
	$expectedTS = $quota_info['qhard']-$quota_info['qused'];
	if ($quota_info['qhard'] - $model['a'] > 0) {
	$expectedTS -= $model['a'];
	}
	$expectedTS /= $model['b'];
	if (0 && $expectedTS > \apps\quotatracker\Page::FORECAST_LIMIT)
	$expectedTS = -1;
	else
	$expectedTS = floor($expectedTS+$_SERVER['REQUEST_TIME']);
	}

	$quota_pct = round($quota_info['qused']/$quota_info['qhard']*100);
	if ($quota_pct >= 85)
	$over_quota_cls = 'ui-gauge-crit';
	else if ($quota_pct >= 65) {
	$over_quota_cls = 'ui-gauge-warn';
	} else {
	$over_quota_cls = 'ui-gauge-normal';
	}
	?>
	<tr class="entry">
		<td class="cell1">
			<span class="key"><?php printf('<span class="key-item" style="border-color:%s"></span>',
			$eqinfo['color']);?></span>
			<?=$user?>
		</td>
		<td class="disk-usage monospace">
			<div class="ui-gauge-cluster <?=$over_quota_cls?>">
				<div class="ui-gauge  hidden-xs-down" id="quota-<?=$user?>">
					<div class="ui-gauge-used ui-gauge-slice"></div>
					<div class="ui-gauge-free ui-gauge-slice"></div>
					<div class="ui-gauge-slice ui-gauge-cap"></div>
				</div>
				<div class="ui-label-cluster">
					<span class="ui-gauge-label  hidden-sm-down ui-label-percentage"><?=$quota_pct;?>%</span>
					<span class="ui-gauge-label ui-label-used"><?=sprintf("%u",$quota_info['qused']/1024)?> MB</span>
					<span class="ui-gauge-label ui-label-total"><?=sprintf("%u",$quota_info['qhard']/1024)?> MB</span>
				</div>
			</div>
		</td>
		<td>
			<?php
                    if ($expectedTS > 0 && $expectedTS < 1e9) {

			$left = floor(($expectedTS-$_SERVER['REQUEST_TIME'])/86400);
			if ($left > 0 && $left < 90) {
			print '<span class="warning">';
                        }
                        if ($left < 0) {
                        	print "&lt;over&gt;";
                        } else if ($left/365 < 3 ) {

                        	print date("F jS, Y", $expectedTS);
                        	if ($left < 180) {
								echo ' &ndash; ' . \Formatter::commafy($left).' days';
                        	}
                        	if ($left < 90) print '</span>';
			}


			} else {
			echo "(n/a)";
			}

			?>
		</td>
		<td class="monospace hidden-sm-down">

			<?php $kb = round($model['b']*86400);
            	if ($kb < 0) { print "<span class='decr'>-".abs($kb)." KB<i class='fa fa-caret-down'></i></span>"; }
			else if ($kb > 0) print "<span class='incr'>+".$kb." KB<i class='fa fa-caret-up'></i></span>";
			?>
		</td>
	</tr>
	<?php
        }
    ?>
</table>
