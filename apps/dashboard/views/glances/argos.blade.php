<div id="argosMarker">
	<div class="d-flex-row">
		<h2 class="title mt-0 service-health d-flex">
			Service health
			<button class="btn btn-outline-primary ml-auto" name="argos-link" id="argosLink" type="button">
				<i class="fa fa-medkit mx-0"></i>
				Test alerts
			</button>
		</h2>
		@include('glances.argos-hot')
	</div>
</div>