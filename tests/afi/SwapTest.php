<?php
	require_once dirname(__DIR__, 1) . '/TestFramework.php';

	class SwapTest extends TestFramework
	{
		public function testModuleSwap()
		{
			\Error_Reporter::set_verbose(0);
			$auth1 = \TestHelpers::create(array_get(Definitions::get(), 'auth.site.domain'));
			$afi1 = apnscpFunctionInterceptor::factory($auth1);
			$this->assertEquals($afi1->common_get_service_value('dns','provider'), $afi1->dns_get_provider());
			$afi1->swap('dns', new class extends Dns_Module {
				public function testvalidatenomethodex() {
					return 'works';
				}
			});
			$this->assertEquals('works', $afi1->dns_testvalidatenomethodex());
			$afi1->reset('dns');
			$oldex = \Error_Reporter::exception_upgrade(Error_Reporter::E_ERROR);
			try {
				$afi1->dns_testvalidatenomethodex();
				$this->fail('DNS module invalidated');
			} catch (\apnscpException $e) {
				$this->assertContains('testvalidatenomethodex', $e->getMessage(), 'Test method no longer present');
			} finally {
				\Error_Reporter::exception_upgrade($oldex);
			}
		}
	}

