<?php
	require_once dirname(__DIR__) . '/TestFramework.php';
	require_once(__DIR__ . '/helpers/RecordFramework.php');

	class ResourceRecordTest extends TestFramework
	{
		const EXCEPTION_LEVEL = \Error_Reporter::E_ERROR;
		protected array $domains = [];
		protected string $name;
		protected ?\Opcenter\Dns\Record $lastRecord;
		protected Closure $taggingFunction;

		public function tearDown()
		{
			(new \Opcenter\Dns\Bulk)->remove(new \Opcenter\Dns\Record('_dummy_zone.com', [
				'name'      => $this->name,
				'rr'        => 'A',
				'parameter' => ''
			]), function (\apnscpFunctionInterceptor $afi, \Opcenter\Dns\Record $r) {
				return in_array($r->getZone(), $this->domains, true);
			});

			$this->lastRecord = null;
			parent::tearDown();
		}

		public function __construct()
		{
			parent::__construct();

			$this->taggingFunction = function (\apnscpFunctionInterceptor $afi, \Opcenter\Dns\Record $r) {
				// only remove the record if server authorized to handle mail
				if ($afi->dns_get_provider() !== 'powerdns') {
					return false;
				}
				$this->domains[] = $r->getZone();
				$this->lastRecord = $r;
			};
		}

		public function setUp()
		{
			parent::setUp();
			$this->domains = [];
			$this->name = \Opcenter\Auth\Password::generate(16, 'a-z0-9');
		}

		public function testBulkAdd()
		{;
			$parameter = '3.5.6.7';
			(new \Opcenter\Dns\Bulk)->add(new \Opcenter\Dns\Record('_dummy_zone.com', [
				'name'      => $this->name,
				'rr'        => 'A',
				'parameter' => $parameter
			]), $this->taggingFunction);

			if (0 === \count($this->domains)) {
				$this->markTestIncomplete('Not enough samples');
			}
			$this->assertEquals($parameter, \Net_Gethost::gethostbyname_t($this->lastRecord->hostname(), 5000, AUTH_PDNS_NS));
		}

		public function testBulkSimpleUpdate()
		{
			$parameter = '3.5.6.7';
			$newParameter = '8.9.1.2';
			(new \Opcenter\Dns\Bulk)->add(new \Opcenter\Dns\Record('_dummy_zone.com', [
				'name'      => $this->name,
				'rr'        => 'A',
				'parameter' => $parameter
			]), $this->taggingFunction);

			(new \Opcenter\Dns\Bulk)->replace(new \Opcenter\Dns\Record('_dummy_zone.com', [
				'name' => $this->name,
				'rr'   => 'A'
			]), function (\apnscpFunctionInterceptor $afi, \Opcenter\Dns\Record $r) use ($newParameter) {

				$r['parameter'] = $newParameter;

				return $afi->dns_get_provider() === 'powerdns';
			});

			if (0 === \count($this->domains)) {
				$this->markTestIncomplete('Not enough samples');
			}

			for ($i = 0; $i < 5; $i++) {
				$ip = \Net_Gethost::gethostbyname_t($this->lastRecord->hostname(), 5000, AUTH_PDNS_NS);
				if ($ip === $newParameter) {
					break;
				}
				sleep(1);
			}

			$this->assertEquals($newParameter, $ip);
		}

		public function testBulkMetaUpdate()
		{
			$parameter = '10 mail.foo.bar.';
			$newParameter = '20 mail.foo.bar.';

			(new \Opcenter\Dns\Bulk)->add(new \Opcenter\Dns\Record('_dummy_zone.com', [
				'name'      => $this->name,
				'rr'        => 'MX',
				'parameter' => $parameter
			]), $this->taggingFunction);

			(new \Opcenter\Dns\Bulk)->replace(new \Opcenter\Dns\Record('_dummy_zone.com', [
				'name' => $this->name,
				'rr'   => 'MX'
			]), function (\apnscpFunctionInterceptor $afi, \Opcenter\Dns\Record $r) {

				$r->setMeta('priority', "20");

				return $afi->dns_get_provider() === 'powerdns';
			});

			if (0 === \count($this->domains)) {
				$this->markTestIncomplete('Not enough samples');
			}

			$afi = \apnscpFunctionInterceptor::factory(\Auth::context(null, $this->lastRecord->getZone()));
			$r = null;
			for ($i = 0; $i < 5; $i++) {
				$records = $afi->dns_get_records($this->lastRecord['name'], $this->lastRecord['rr'], $this->lastRecord->getZone());
				foreach ($records as &$r) {
					if ($r['parameter'] === $newParameter) {
						break;
					}
				}
				sleep(1);
			}

			$this->assertEquals($newParameter, $r['parameter']);
		}
	}

