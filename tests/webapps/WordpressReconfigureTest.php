<?php
    require_once dirname(__DIR__, 1) . '/TestFramework.php';

    class WordpressReconfigureTest extends TestFramework
    {
        protected $filename;

        public function testReconfigure()
        {
            $auth1 = \TestHelpers::create(array_get(Definitions::get(), 'auth.site.domain'));
            $afi1 = apnscpFunctionInterceptor::factory($auth1);

            $subdomain1 = "test-" . uniqid();
            $afi1->web_add_subdomain($subdomain1, '/var/www/' . $subdomain1);
            $this->assertTrue($afi1->wordpress_install($subdomain1, '', [
                'version' => array_get(Definitions::get(), 'webapps.wordpress.install_version', '4.9.12'),
                'ssl'     => false,
                'notify'  => false
            ]));

            $this->assertEquals(
                $afi1->wordpress_get_version($subdomain1),
                array_get(Definitions::get(), 'webapps.wordpress.install_version', '4.9.12')
            );


            $this->cleanup($afi1, $subdomain1);
        }

        protected function cleanup(\apnscpFunctionInterceptor $afi, string $subdomain) {
            $this->assertTrue($afi->wordpress_uninstall($subdomain));
            $this->assertTrue($afi->web_remove_subdomain($subdomain));
            $afi->file_delete('/var/www/' . $subdomain, true);
        }
    }

