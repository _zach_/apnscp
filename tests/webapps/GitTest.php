<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, June 2020
 */

	use Module\Support\Webapps\UpdateAssetCandidate;

	require_once dirname(__DIR__, 1) . '/TestFramework.php';

class GitTest extends TestFramework {

	public function testGitFeature() {
		$ctx = TestHelpers::create(array_get(Definitions::get(), 'auth.site.domain'));
		\Opcenter\Http\Apache::buildConfig('now', true);
		\Opcenter\Http\Apache::waitRebuild();

		$this->setApnscpFunctionInterceptor(\apnscpFunctionInterceptor::factory($ctx));
		$subdomain = 'wp-test-' . mt_rand(0, PHP_INT_MAX) . '.' . $ctx->domain;
		$this->assertTrue($this->getApnscpFunctionInterceptor()->web_add_subdomain($subdomain, '/var/www/' . $subdomain));
		$this->assertTrue(
			$this->getApnscpFunctionInterceptor()->wordpress_install(
				$subdomain,
				'',
				[
					'version' => array_get(Definitions::get(), 'webapps.wordpress.install_version'),
					'notify'  => false,
					'ssl'     => false,
					'email'   => null,
					'git'     => true,
				]
			)
		);

		defer($_, static function() use($ctx, $subdomain) {
			\apnscpFunctionInterceptor::factory($ctx)->wordpress_uninstall($subdomain);
		});

		$meta = \Module\Support\Webapps\MetaManager::factory($ctx)->get("/var/www/$subdomain");
		$git = \Module\Support\Webapps\Git::instantiateContexted($ctx, [
			"/var/www/$subdomain",
			$meta
		]);

		$this->assertTrue($git->enabled());
		$afi = apnscpFunctionInterceptor::factory($ctx);
		$this->assertNotEmpty($oldCount = $afi->git_list_commits("/var/www/$subdomain"));
		$this->assertSame(
			array_get(Definitions::get(), 'webapps.wordpress.install_version'),
			$afi->wordpress_get_version($subdomain, '')
		);
		$afi->file_put_file_contents("/var/www/$subdomain/unit-test.test", "bar");
		$updater = \Module\Support\Webapps\UpdateCandidate::instantiateContexted($ctx,[$subdomain, '']);
		$updater->parseAppInformation($meta);
		$assurance = $updater->initializeAssurance($git);
		$count = $afi->git_list_commits("/var/www/$subdomain");
		$this->assertGreaterThan($oldCount, $count, 'Assurance triggers snapshot');
		$updater->forceUpdateVersion(array_get(Definitions::get(), 'webapps.wordpress.next_version'));
		$updater->process();
		$this->assertNotEquals(
			array_get(Definitions::get(), 'webapps.wordpress.install_version'),
			$afi->wordpress_get_version($subdomain, '')
		);

		$this->assertTrue($assurance->assert());

		$assurance->rollback();
		$this->assertSame(
			array_get(Definitions::get(), 'webapps.wordpress.install_version'),
			$afi->wordpress_get_version($subdomain, ''),
			'Test rollback'
		);

		$this->assertSame($count, $afi->git_list_commits("/var/www/$subdomain"));
	}

	public function testPartialRollbackFeature()
	{
		/**
		 * Requires manually overriding wordpress:update-plugins
		 */
		$plugins = [
			'wordfence'      => '7.3.4',
			'w3-total-cache' => '0.13.2',
			'woocommerce'    => '5.1.2',
		];

		$ctx = TestHelpers::create(array_get(Definitions::get(), 'auth.site.domain'));
		\Opcenter\Http\Apache::buildConfig('now', true);
		\Opcenter\Http\Apache::waitRebuild();
		$afi = \apnscpFunctionInterceptor::factory($ctx);
		$afi->swap('wordpress', new class extends Wordpress_Module {
			protected function execCommand(?string $path, string $cmd, array $args = [], array $env = [])
			{
				$ret = parent::execCommand($path, $cmd, $args);

				if (false === strpos($cmd, 'plugin update')) {
					return $ret;
				}
				if ($args['name'] === 'w3-total-cache') {
					// sabotage update
					$this->getApnscpFunctionInterceptor()->file_put_file_contents($path . '/wp-blog-header.php', '<?php sdfjsdfh3*(;');
				}
				return $ret;
			}
		});
		$this->setApnscpFunctionInterceptor($afi);
		$this->assertSame($afi, \apnscpFunctionInterceptor::factory($ctx));
		$subdomain = 'wp-test-' . mt_rand(0, PHP_INT_MAX) . '.' . $ctx->domain;
		$this->assertTrue($this->getApnscpFunctionInterceptor()->web_add_subdomain($subdomain,
			'/var/www/' . $subdomain));

		$this->assertTrue(
			$this->getApnscpFunctionInterceptor()->wordpress_install(
				$subdomain,
				'',
				[
					'version' => array_get(Definitions::get(), 'webapps.wordpress.install_version'),
					'notify'  => false,
					'ssl'     => false,
					'email'   => null,
					'git'     => true,
				]
			)
		);

		defer($_, static function () use ($ctx, $subdomain) {
			\apnscpFunctionInterceptor::factory($ctx)->wordpress_uninstall($subdomain);
		});

		foreach($plugins as $name => $version) {
			$this->assertTrue($afi->wordpress_install_plugin($subdomain, '', $name, $version));
			$this->assertEquals(
				$version,
				array_get($afi->wordpress_plugin_status($subdomain, '', $name), 'version')
			);
		}

		$this->assertTrue(\Module\Support\Webapps\App\Loader::fromHostname(null, $subdomain, '', $ctx)->getOption('git'), 'git enabled');
		$meta = \Module\Support\Webapps\MetaManager::factory($ctx)->get("/var/www/$subdomain");
		$git = \Module\Support\Webapps\Git::instantiateContexted($ctx, [
			"/var/www/$subdomain",
			$meta
		]);
		$updater = \Module\Support\Webapps\UpdateAssetCandidate::instantiateContexted($ctx, [$subdomain, '']);
		// @FIXME not pulled from global repo?

		$updater->parseAppInformation($meta);
		$assurance = $updater->initializeAssurance($git);
		$count = $afi->git_list_commits("/var/www/$subdomain");

		$types = array_keys(UpdateAssetCandidate::KNOWN_ASSETS);
		$this->assertNotNull($assurance);
		$this->assertGreaterThan(0, $count);
		foreach ($types as $type) {
			$updater->setAssetType($type);
			foreach ($updater->getAssets() as $assetName => $assetInfo) {
				if ($assetInfo['current']) {
					continue;
				}
				if (!isset($plugins[$assetName])) {
					continue;
				}
				$updateCandidate = clone $updater;
				$updateCandidate->setAssetName($assetName);
				$updateCandidate->setAvailableVersions([$assetInfo['version'], $assetInfo['max']]);
				$updateCandidate->setVersion($assetInfo['version']);
				$updateCandidate->process();
				$fn = $assetName !== 'w3-total-cache' ? 'assertTrue' : 'assertFalse';
				// $updateCandidate->validateAssurance()
				$this->$fn($updateCandidate->validateAssurance(), "Plugin ${assetName} as ${fn}");
				$fn = $assetName === 'w3-total-cache' ? 'assertEquals' : 'assertNotEquals';
				$this->$fn(
					$plugins[$assetName],
					array_get($afi->wordpress_plugin_status($subdomain, '', $assetName), 'version'),
					"$assetName version $fn"
				);
			}
		}
	}
}