<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, March 2020
 */


	use Opcenter\Auth\Password;

	require_once dirname(__DIR__, 1) . '/TestFramework.php';

	class MassServiceRenameTest extends TestFramework
	{
		const EXCEPTION_LEVEL = \Error_Reporter::E_ERROR;

		public function testRenames() {
			$creds = [
				'domain' => [
					Password::generate(16, 'a-z0-9') . '.test',
					Password::generate(16, 'a-z0-9') . '.test'
				],
				'prefix' => [
					Password::generate(8, 'a-z0-9'),
					Password::generate(8, 'a-z0-9')
				],
				'admin' => [
					Password::generate(12, 'a-z0-9'),
					Password::generate(12, 'a-z0-9'),
				],
				'aliases' => [
					[
						Password::generate(16, 'a-z0-9') . '.test',
						Password::generate(16, 'a-z0-9') . '.test',
						Password::generate(16, 'a-z0-9') . '.test',
					]
				]
			];

			$creds['domain'][1] = array_get($creds, "aliases.0.0");
			$creds['aliases'][1] = array_merge(array_slice($creds['aliases'][0], 1), (array)$creds['domain'][0]);

			$ephemeral = \Opcenter\Account\Ephemeral::create([
				'mysql.enabled' => true,
				'pgsql.enabled' => true,
				'mail.enabled'  => true,
				'mail.provider' => 'builtin',
				'siteinfo.domain'     => array_get($creds, 'domain.0'),
				'siteinfo.admin_user' => array_get($creds, 'admin.0'),
				'aliases.aliases'     => array_get($creds, 'aliases.0'),
				'mysql.dbaseprefix'   => array_get($creds, 'prefix.0'),
				'pgsql.dbaseprefix'   => array_get($creds, 'prefix.0'),
			]);

			$this->assertInstanceOf(\Opcenter\Account\Ephemeral::class, $ephemeral);

			$admin = \apnscpFunctionInterceptor::factory(\Auth::context(TestHelpers::getAdmin()));
			$ctx = $ephemeral->getContext();
			$afi = $ephemeral->getApnscpFunctionInterceptor();
			$afi->mysql_create_database('foo');
			$afi->pgsql_create_database('foo');
			$afi->email_add_virtual_transport(array_get($creds, 'domain.0'));
			$this->assertTrue($afi->email_add_alias('fwd-test', array_get($creds, 'domain.0'), 'site_blackhole'));
			$this->assertTrue($afi->mysql_add_user('bar', 'localhost', Password::generate()));
			$this->assertTrue($afi->pgsql_add_user('bar', Password::generate()));

			$this->assertTrue($admin->admin_edit_site($ephemeral->getContext()->site, [
				'siteinfo.domain'   => array_get($creds, 'domain.1'),
				'aliases.aliases'   => array_get($creds, 'aliases.1'),
				'mysql.dbaseprefix' => array_get($creds, 'prefix.1'),
				'pgsql.dbaseprefix' => array_get($creds, 'prefix.1'),
			]), 'Rename succeeds');
			$account = $ctx->getAccount();
			$this->assertNotEquals(array_get($creds, 'domain.1'), $account->cur['siteinfo']['domain'], 'Validate mismatch prior to flush');

			// invalidated on edit, update context
			$ctx = \Auth::context(null, $ctx->site);
			$account = $ctx->getAccount()->reset($ephemeral->getContext());
			$this->assertTrue(\apnscpSession::init()->exists($ctx->id));
			$afi = \apnscpFunctionInterceptor::factory($ctx);
			$this->assertTrue($afi->email_address_exists('fwd-test', array_get($creds, 'domain.0')));
			$this->assertEquals(array_get($creds, 'domain.1'), $account->cur['siteinfo']['domain']);
			foreach (array_get($creds, 'aliases.1') as $domain) {
				$this->assertContains($domain, $account->cur['aliases']['aliases'], 'Alias exists');
			}

			$this->assertSame(array_get($creds, 'prefix.1') . '_', $afi->mysql_get_prefix(), 'Database prefix updated');
			$this->assertArrayHasKey(array_get($creds, 'prefix.1') . '_bar', $afi->mysql_list_users());
			$this->assertArrayHasKey(array_get($creds, 'prefix.1') . '_bar', $afi->pgsql_list_users());

		}
	}