#!/usr/bin/env apnscp_php
<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	use Opcenter\Account\DomainOperation;
	use Opcenter\Account\Edit;

	define('INCLUDE_PATH', dirname(__DIR__));
	include INCLUDE_PATH . '/lib/CLI/cmd.php';

	$args = [];
	DomainOperation::preExecutionHook($args);

	\Opcenter\Lock::lock();
	$args = Opcenter\CliParser::parse();
	$sitespec = array_get($args, 'command.0', []);

	if (array_get($args, 'options.all')) {
		if ($sitespec) {
			fatal("Both --all and site specifier may not be provided");
		}
		if (!($sites = \Opcenter\Account\Enumerate::sites())) {
			$sites = \Opcenter\Map::load(\Opcenter\Map::DOMAIN_TXT_MAP);
			if (empty($sites->fetchAll())) {
				warn("No sites found in %s - corrupted map?", \Opcenter\Map::DOMAIN_MAP);
				exit;
			}
			fatal("No sites found in %s - corrupted map?", \Opcenter\Map::DOMAIN_MAP);
		}
	} else {
		$sites = \Opcenter\CliParser::getSiteFromArgs(...array_get($args, 'command'));
	}

	$sites = array_cardinal(array_map(static function ($spec) {
		$site = 'site' . \Auth::get_site_id_from_anything($spec);
		if ($site === 'site') {
			warn("unknown site/domain/identifier `%s'", $spec);

			return null;
		}

		return $site;
	}, $sites));

	if (!$sites) {
		fatal("unknown site/domain/identifier `%s'", $sitespec);
	}

	$status = 0;
	$siteBuffers = [];
	$stats = [
		'succeed'  => 0,
		'failed'   => 0,
		'duration' => 0,
		'skipped'  => 0,
	];
	$oldTime = microtime(true);
	foreach ($sites as $site) {
		$newTime = microtime(true);
		$stats['duration'] += ($newTime - $oldTime);
		$oldTime = $newTime;

		$oldex = \Error_Reporter::exception_upgrade(\Error_Reporter::E_FATAL);
		try {
			$c = cli\cmd(null, $site);
		} catch (\apnscpException $e) {
			$stats['skipped']++;
			if (array_get($args, 'options.all')) {
				debug("Skipping %s: %s", $site, $e->getMessage());
				// ignore when --all specified
				continue;
			}
			throw $e;
		} finally {
			\Error_Reporter::exception_upgrade($oldex);
		}

		if (!$c) {
			fatal('Failed to create CLI object');
		}

		$instance = new Edit($site, array_get($args, 'options.conf', []), array_except($args['options'], ['conf'], []));
		if (array_get($args, 'options.reconfig', false)) {
			$instance->setOption('reconfig', true);
		}

		if (array_has($args, 'options.force')) {
			$instance->setOption('force', true);
		}

		if (array_has($args, 'options.reset-plan')) {
			$instance->setOption('reset', true);
		}

		if (array_has($args, 'options.backup')) {
			$instance->setOption('backup', true);
		}

		if (array_has($args, 'options.dry')) {
			$instance->setOption('dry', true);
		}

		if (null !== ($plan = array_get($args, 'options.plan'))) {
			$instance->setOption('plan', $plan);
		}

		$oldex = \Error_Reporter::exception_upgrade(\Error_Reporter::E_FATAL);
		try {
			$ret = $instance->exec();
		} catch (\Throwable $e) {
			\Error_Reporter::handle_exception($e);
		} finally {
			// "edit" hooks are embedded in Opcenter/Account/Edit
			$domain = $instance->getDomain();
			$status = max($status, $instance->getStatus());
			dlog('Edited %s, %s (%s) %s',
				\Auth::get_domain_from_site_id((int)substr($site, "4")),
				$domain,
				strtoupper(Error_Reporter::error_type(Error_Reporter::get_severity()) ?: 'success'),
				$instance->getStatus() === DomainOperation::RC_FAILURE ? 'Failed' : 'Succeeded'
			);
		}

		$instance->getStatus() === DomainOperation::RC_FAILURE ? $stats['failed']++ : $stats['succeed']++;
		$siteBuffers[$site] = \Error_Reporter::flush_buffer();
		\Error_Reporter::exception_upgrade($oldex);
	}

	// restore buffer for result log
	array_map(static function ($buf) {
		\Error_Reporter::set_buffer($buf);
	}, $siteBuffers);
	if (count($sites) > 1) {
		$count = $stats['succeed'] + $stats['failed'] + $stats['skipped'];
		$msg = "Edited %(count)d sites in %(duration).2fs (%(avg).2fs each) %(succeed)d succeeded, %(failed)d failed, %(skipped)d skipped.";
		$args = [$stats + ['count' => $count, 'avg' => $stats['duration'] / $count]];
		dlog($msg, ...$args);
		info($msg, ...$args);
	}
	exit ((int)$status);