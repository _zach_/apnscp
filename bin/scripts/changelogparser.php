<?php
$fp = fopen('php://stdin', 'r');
$changes = array(
	'REL' => array(),
	'SEC' => array(),
	'NEW' => array(),
	'FIX' => array(),
	'CHG' => array(),
	'REM' => array()
);
while (false !== ($line = fgets($fp))) {
	if (preg_match('/^\s*(' . implode('|', array_keys($changes)) . '):(.*)$/', $line, $match)) {
		$type = $header = strtoupper($match[1]);
		if ($type == 'NEW') {
			$header = '<b>' . $type . '</b>';
		} else if (false && $type == 'FIX') {
			$header = '<i>' . $type . '</i>';
		}

		$changes[$type][] = $header . ': ' . trim($match[2]);
	}
}

print '<UL>' . "\n";
foreach ($changes as $set) {
	foreach ($set as $item) {
		print '<li>' . $item . "</li>\n";
	}
	print "\n\n";
}
print '</UL>' . "\n\n";
