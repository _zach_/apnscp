#!/usr/bin/env apnscp_php
<?php

	/**
	 * Check for consistency in uid, gid mapping into database
	 */
	use function cli\parse;

	include(dirname(__FILE__, 3) . '/lib/CLI/cmd.php');

	$old = \Error_Reporter::set_verbose();
	\Error_Reporter::set_verbose(max($old, 1));

	$args = parse();

	$collection = \Illuminate\Support\LazyCollection::make(function () {
		foreach (\Opcenter\Account\Enumerate::sites() as $site) {
			if (null === ($context = Auth::nullableContext(null, $site))) {
				continue;
			}

			yield \apnscpFunctionInterceptor::factory($context);
		}
	});

	$collection->each(function (\apnscpFunctionInterceptor $afi) {
		$db = PostgreSQL::pdo();
		// check GID presence
		$site_id = \Auth::get_site_id_from_domain($afi->common_get_service_value('siteinfo', 'domain'));
		info("Checking site%d", $site_id);
		$rs = $db->query("SELECT gid FROM gids WHERE site_id = " . $site_id);
		if (!$rs->rowCount()) {
			$pwd = posix_getpwnam($afi->common_get_service_value('siteinfo', 'admin'));
			if (!$pwd) {
				warn("User %s missing from /etc/passwd - cannot regenerate - run mapCheck",
					$afi->common_get_service_value('siteinfo', 'admin'));
				return;
			}
			(new \Opcenter\Database\PostgreSQL\Opcenter(\PostgreSQL::pdo()))->createGroup(
				$site_id,
				$pwd['gid']
			);
			warn("Adding gid %(gid)d for %(siteid)d", [
				'gid' => $pwd['gid'],
				'siteid' => $site_id
			]);
		}

		/** @var \Opcenter\Database\PostgreSQL\Generic\User $queryTranslator */
		$queryTranslator = \Opcenter\Database\PostgreSQL::vendor('user');
		foreach ($afi->user_get_users() as $username => $pwd) {
			$rs = $db->query("SELECT \"user\", site_id FROM uids WHERE uid = " . $pwd['uid']);
			if ($rs && $rs->rowCount()) {
				$result = $rs->fetchObject();
				if ($result->user === $username && $result->site_id === $site_id) {
					continue;
				}

				if ($result->site_id === $site_id) {
					warn("found dangling user: %(old)s => %(new)s",
						['old' => $result->user, 'new' => $username]
					);
					// easy case, user rename dangling
					$query = $queryTranslator->renameUser($pwd['uid'], $username, $site_id);
				} else {
					fatal("uid %(uid)d bound to wrong site id/user: %(siteid)s/%(user)s", [
						'uid' => $pwd['uid'], 'siteid' => $site_id, 'user' => $username
					]);
				}
			} else {
				warn("uid %(uid)d missing for site id/user: %(siteid)s/%(user)s", [
					'uid' => $pwd['uid'], 'siteid' => $site_id, 'user' => $username
				]);
				$query = $queryTranslator->saveUid($site_id, $pwd['uid'], $username);
			}

			if (!$db->exec($query)) {
				error("Failed to add %(uid)d: (%(errcode)s) %(err)s", [
						'uid' => $pwd['uid'],
						'errcode' => $db->errorCode(),
						'err' => $db->errorInfo()[2]
				]);
			}

		}
	});

	exit(!\Error_Reporter::is_error());
