/var/log/php-fpm/*.log {
	create
	missingok
	notifempty
	nosharedscripts
	postrotate
		/bin/kill -SIGUSR1 $(cat /var/run/php-fpm/$(basename ${1%%.log}).pid) 2> /dev/null || true
	endscript
}
