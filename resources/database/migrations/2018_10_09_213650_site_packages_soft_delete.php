<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Schema\Blueprint;

use Illuminate\Database\Migrations\Migration;

class SitePackagesSoftDelete extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	if (Schema::connection('pgsql')->hasTable('site_packages_view')) {
    		return;
		}
		Schema::connection('pgsql')->table('site_packages', function (Blueprint $table) {
			if (!\in_array('deleted', $table->getColumns())) {
				$table->boolean('deleted')->default('f');
			}
		});
		DB::connection('pgsql')->getPdo()->exec('CREATE VIEW site_packages_view AS 
			(SELECT package_name, version, service, release, arch FROM site_packages WHERE deleted = \'f\')
		');
	}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		DB::connection('pgsql')->getPdo()->exec('DROP VIEW IF EXISTS site_packages_view');
		Schema::connection('pgsql')->table('site_packages', function (Blueprint $table) {
			$table->dropColumn('deleted');
		});
    }
}
