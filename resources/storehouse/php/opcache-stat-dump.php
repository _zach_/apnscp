<?php

if (!extension_loaded('Zend OPcache')) {
	exit;
}

echo serialize(array_merge(opcache_get_configuration(), opcache_get_status(false)));