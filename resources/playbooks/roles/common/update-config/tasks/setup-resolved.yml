# Use systemd-resolved if enabled or canonical_resolv: False
---
- name: Query if systemd-resolved active
  command:
    systemctl show -pActiveState systemd-resolved
  failed_when: false
  changed_when: false
  register: s
- set_fact: update_resolv="{{ '=inactive' in s.stdout }}"
  when: canonical_resolv == 'auto'
- block:
    # via CentOS #16988, witnessed on C8.2019, fixed in systemd 239-40
    # https://bugs.centos.org/view.php?id=16988
  - name: Apply ProtectSystem= usage
    include_role: name=systemd/override-config
    vars:
      service: systemd-resolved
      config:
        - group: Service
          vars:
            ProtectSystem: "strict"
            PrivateTmp: "yes"
    when: ansible_distribution_major_version == '8'

  - name: Start systemd-resolved
    systemd: name=systemd-resolved state=started enabled=yes
  - name: Enable systemd-resolved preset
    include_role: name=systemd/preset
    vars:
      service: systemd-resolved
      enabled: True
      start: True
  when: not update_resolv

  # Witnessed on fastpipe.io, falling back to dns using advertised
  # nameservers times out on FQDN lookup that impacts hostname -f
  # blocking normal startup of ApisCP
  # via nss-resolve(8):
  # It should be before the "files" entry, since systemd-resolved supports /etc/hosts internally, but with caching.
- name: Promote resolve usage
  lineinfile:
    path: /etc/nsswitch.conf
    regexp: '^[^#]*hosts:\s*((?:(?!resolve).)*)$'
    backrefs: true
    line: 'hosts: resolve \1'
- name: Set DNS configuration
  ini_file:
    path: /etc/systemd/resolved.conf
    section: Resolve
    option: "{{ item.key }}"
    value: "{{ item.value }}"
    no_extra_spaces : True
  with_dict: "{{ resolved_configuration }}"
  when: not update_resolv
  notify: Restart systemd-resolved
  # Some images enable systemd-resolved, but fail to link to /etc/resolv.conf
- name: Disable DNSSEC for PTR lookup
  file:
    path: "{{ dnssec_anchor_path }}"
    state: directory
    owner: systemd-resolve
    group: systemd-resolve
    mode: 0700
- name: Template negative trust anchor in {{ negative_trust_anchor_name }}
  copy:
    dest: "{{ dnssec_anchor_path }}/{{ negative_trust_anchor_file }}"
    content: "{{ negative_trust_anchor_template }}"
  notify: Restart systemd-resolved
- name: Relink broken resolv.conf
  file:
    path: /etc/resolv.conf
    state: link
    src: "/run/systemd/resolve/{{ resolved_use_stub | ternary('stub-resolv', 'resolv') }}.conf"
    force: yes
  when: not update_resolv
  notify: Restart systemd-resolved
