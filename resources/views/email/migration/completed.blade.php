@component('mail::message')
@section("title", "Migration Completed - " . $domain)
Hello,<br/><br/>
<p>
	Account migration has been completed. New account is live on {{ $newserver }}.
	@if (defined('MISC_CP_PROXY') && !empty(MISC_CP_PROXY))
	To login to the control panel within the next 24 hours, please visit <a href="{{ $panelurl }}">{{ $panelurl }}</a>.
	After 24 hours you may continue to use <a href="{{ constant('MISC_CP_PROXY') }}">{{ constant('MISC_CP_PROXY') }}</a>.
	@else
		You may login to the new control panel at <a href="{{ $panelurl }}">{{ $panelurl }}/</a>.
	@endif
	Your account on the old server, {{ $oldserver }}, has been suspended.
</p>
<p>
	A migration log has been included for your records.
</p>
@endcomponent

