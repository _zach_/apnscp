@php
	/** @var \Opcenter\Bandwidth\Overage $overage */
@endphp

@component('email.indicator', ['status' => 'warning'])
	Your attention is required.
@endcomponent

@component('mail::message')
Hello,

The following domain has exceeded its bandwidth limit for the period beginning **{{ date('Y-m-d', $overage->getBegin()) }}**
and ending **{{ date('Y-m-d', $overage->getEnd()) }}**.

Your site has consumed {{ sprintf('%.2f', Formatter::changeBytes($overage->getTotal(), 'GB', 'B')) }} GB over your account limit
{{ Formatter::changeBytes($overage->getThreshold(), 'GB', 'B') }} GB.

For detailed information, see **Reports** > **Bandwidth Breakdown** within the [control panel]({{ \Auth_Redirect::getPreferredUri() }}).
Examining individual log entries in `/var/log/httpd/access_log` may be of use as well.

@if ($overage->triggersSuspension())
Your site has been suspended automatically to prevent further abuse of service. The suspension
will automatically end in {{ (new DateTime())->setTimestamp($overage->getEnd())->diff(new DateTime())->days+1 }} days
when the new bandwidth cycle begins.
@endif

@endcomponent

