<!doctype html>
<html>
<head>
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>@yield('title', "Hello!")</title>
    <style>
        @inline(email.css)
    </style>
</head>
<body style="color: #000 !important;">
<table class="body-wrap">
    <tr>
        <td colspan="3">
            <h1 id="logo" style="height:48px;width: 200px;display: block;margin: 5px auto 0 auto; text-align: center;">
                <img src="{{ \Auth_Redirect::getPreferredUri() }}/images/logo-badge.png" class="emailImage"
                     style="height:54px !important; margin: 0 auto; text-align: center;"/>
            </h1>
        </td>
    </tr>
    <tr>
        <td></td>
        <td class="container" width="600">
            <div class="content">
                @yield("content")

                <div class="footer">
                    @yield("footer")
                </div>
            </div>
        </td>
        <td></td>
    </tr>
</table>

</body>
</html>
