<div id="ui-loading" class="position-absolute w-100 d-flex align-items-center h-100">
	<i class="indicator ui-ajax-indicator ui-ajax-loading ui-ajax-loading-large mx-auto"></i>
	{{-- Optional loading screen while app processes --}}
	<script>
		$(window).on('load', function () {
			$('#ui-loading .indicator').remove();
		});
	</script>
</div>
