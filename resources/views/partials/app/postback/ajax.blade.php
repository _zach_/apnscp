<div id="remote-pb-container">
	<section class="alert mb-0 pt-2 alert-dismissible template">
		<div class="ui-pb-msg-header">
			<h6 class="ml-0 mb-0 title">Postback Status</h6>
			<hr />
			<button type="button" class="close p-0 pr-2 text-right" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<p class="msg">Postback failed!!!</p>
	</section>
</div>