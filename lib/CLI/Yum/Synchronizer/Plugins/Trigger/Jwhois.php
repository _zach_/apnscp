<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, October 2017
	 */

	namespace CLI\Yum\Synchronizer\Plugins\Trigger;

	use CLI\Yum\Synchronizer\Plugins\AlternativesTrigger;

	/**
	 * Class Binutils
	 *
	 * @package CLI\Yum\Synchronizer\Plugins\Trigger
	 *
	 * @todo    support other packages
	 *
	 */
	class Jwhois extends AlternativesTrigger
	{
		/**
		 * @var array
		 */
		protected $alternatives = [
			[
				'name'     => 'whois',
				'src'      => '/usr/bin/whois',
				'dest'     => '/usr/bin/jwhois',
				'priority' => 60
			],
			[
				'name'     => 'whois-man',
				'src'      => '/usr/share/man/man1/whois.1.gz',
				'dest'     => '/usr/bshare/man/man1/whois.jwhois.1.gz',
				'priority' => 50
			],

		];
	}