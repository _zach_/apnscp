<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, September 2019
 */

namespace Opcenter\Crypto\Letsencrypt\DispatchServices;

use AcmePhp\Core\AcmeClient;
use AcmePhp\Core\Exception\AcmeCoreServerException;
use AcmePhp\Core\Protocol\AuthorizationChallenge;
use AcmePhp\Ssl\Certificate;
use AcmePhp\Ssl\KeyPair;
use Opcenter\Crypto\Letsencrypt;
use Opcenter\Crypto\Letsencrypt\DispatchServices\Acmephp\Client;
use Opcenter\Crypto\Letsencrypt\DispatchServices\Acmephp\KeypairFinder;

class Acmephp {

	/**
	 * Create secure HTTP client
	 *
	 * @param KeyPair $pair
	 * @return AcmeClient
	 */
	public static function client(\Auth_Info_User $ctx): AcmeClient {
		$pair = static::getOrMakeRegistrationKeyPair($ctx);
		return  (new Client($pair))->get();
	}

	/**
	 * Get registration keys
	 *
	 * @param \Auth_Info_User $ctx
	 * @param string          $type
	 * @return KeyPair
	 */
	private static function getOrMakeRegistrationKeyPair(\Auth_Info_User $ctx, $type = KeypairFinder::DEFAULT_KEY): KeyPair {
		return (new KeypairFinder($ctx, $type))->getRegistrationKey();
	}

	/**
	 * Register Let's Encrypt account
	 *
	 * @param \Auth_Info_User $ctx
	 * @param string|null     $email optional notification email, unused
	 * @return bool
	 */
	public static function register(\Auth_Info_User $ctx, string $email = null): bool
	{
		$client = static::client($ctx);
		try {
			$status = $client->registerAccount($email);
		} catch (AcmeCoreServerException $e) {
			$method = new \ReflectionMethod($client, 'getHttpClient');
			$method->setAccessible(true);
			$driver = $method->invoke($client);
			$rfxn = new \ReflectionProperty($driver, 'lastResponse');
			$rfxn->setAccessible(true);
			$response = json_decode($rfxn->getValue($driver)->getBody()->getContents(), true);
			return error("Failed to register: %s", $response['detail'] ?? $e->getMessage());
		}

		return array_get($status, 'status', 'unknown') === 'valid';
	}

	/**
	 * Get AcmePHP certificate
	 *
	 * @param string $marker
	 * @return Certificate|null
	 */
	public static function getCertificate(string $marker): ?Certificate
	{
		$cert = Letsencrypt::getCertificateComponentData($marker);
		if (!$cert) {
			return null;
		}

		return new Certificate($cert['crt'], new Certificate($cert['chain']));
	}

	/**
	 * @param AuthorizationChallenge[] $challenges
	 * @param array           $preferredMode preferred challenge (if any)
	 * @return AuthorizationChallenge[]
	 */
	public static function orderChallenges(array $challenges, array $preferredMode): array
	{
		$modes = array_flip(array_unique($preferredMode + append_config(Letsencrypt\AcmeDispatcher::VALIDATION_MODES)));
		// assume outstanding challenge is keyed to domain??
		usort($challenges, static function (AuthorizationChallenge $a, AuthorizationChallenge $b) use ($modes) {
			$modeA = strtok($a->getType(), '-');
			$modeB = strtok($b->getType(), '-');
			if (!isset($modes[$modeA])) {
				return 3;
			}
			if (!isset($modes[$modeB])) {
				return -1;
			}

			return $modes[$modeA] <=> $modes[$modeB];
		});
		return $challenges;
	}
}
