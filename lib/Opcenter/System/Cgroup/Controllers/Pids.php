<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, July 2018
	 */

	namespace Opcenter\System\Cgroup\Controllers;

	use Opcenter\Filesystem;
	use Opcenter\Process;
	use Opcenter\System\Cgroup\Controller;

	class Pids extends Controller
	{
		protected const ATTRIBUTES = [
			'proclimit' => 'pids.max'
		];

		public const LOGGABLE_METRICS = [
			'used' => [
				'label'   => 'Processes',
				'type'    => 'value',
				'counter' => 'pids.current'
			],
		];

		/**
		 * Read PID count
		 *
		 * @param string $path
		 * @return string
		 */
		protected function readRawCounter(string $path): string
		{
			if (file_exists($path)) {
				return parent::readRawCounter($path);
			}

			// probably admin!
			if (false === ($dh = opendir('/proc'))) {
				return '';
			}
			$cnt = 0;
			while (false !== ($entry = readdir($dh))) {
				if (!is_dir("/proc/${entry}")) {
					continue;
				}
				if (\strlen($entry) === strspn($entry, '0123456789')) {
					$cnt++;
				}
			}
			closedir($dh);
			return (string)$cnt;
		}


	}
