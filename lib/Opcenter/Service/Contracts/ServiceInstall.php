<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Service\Contracts;

	use Opcenter\SiteConfiguration;

	/**
	 * Interface ServiceInstall
	 *
	 * Calls populate() and depopulate() methods when
	 * corresponding service parameter "enabled" is changed
	 *
	 * @package Opcenter\Service\Contracts
	 */
	interface ServiceInstall
	{
		public function populate(SiteConfiguration $svc): bool;

		public function depopulate(SiteConfiguration $svc): bool;
	}