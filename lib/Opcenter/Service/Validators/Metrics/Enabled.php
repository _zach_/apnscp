<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */


	namespace Opcenter\Service\Validators\Metrics;

	use Daphnie\Chunker;
	use Daphnie\Collector;
	use Daphnie\Connector;
	use Event\Cardinal;
	use Event\Events;
	use Opcenter\Service\Contracts\ServiceInstall;
	use Opcenter\Service\Contracts\ServiceReconfiguration;
	use Opcenter\SiteConfiguration;

	class Enabled extends \Opcenter\Service\Validators\Common\Enabled implements ServiceInstall, ServiceReconfiguration
	{
		public const DESCRIPTION = 'Record per-site metrics';

		public function valid(&$value): bool
		{
			if (!TELEMETRY_ENABLED) {
				warn("Telemetry feature disabled on server. Disabling metrics collection");
				$value = 0;
			}
			return parent::valid($value);
		}

		public function populate(SiteConfiguration $svc): bool
		{
			$collector = (new Collector(\PostgreSQL::pdo()));
			$collector->toggleCollection($svc->getSiteId(), true);
			return true;
		}

		public function reconfigure($old, $new, SiteConfiguration $svc): bool
		{
			if ($old !== $new && !$new) {
				return $this->depopulate($svc);
			}

			return $this->populate($svc);
		}

		public function rollback($old, $new, SiteConfiguration $svc): bool
		{
			if ($old === $new) {
				return true;
			}
			return $this->reconfigure($new, $old, $svc);
		}


		public function depopulate(SiteConfiguration $svc): bool
		{
			$this->checkForTelemetryCompression();
			debug("Deleting historical metrics");

			\Cronus::suspend();
			Cardinal::register(['*', Events::END], static function (string $event, SiteConfiguration $svc) {
				\Cronus::resume();
			});

			$collector = (new Collector(\PostgreSQL::pdo()));

			if ($this->ctx->isDelete()) {
				$collector->forgetSite($svc->getSiteId());
				return true;
			}

			$collector->toggleCollection($svc->getSiteId(), false);

			if (!$collector->deleteSite($svc->getSiteId())) {
				// gets doubly deleted at siteinfo removal
				return warn("Failed to delete metrics for %(site)s: %(err)s",
				[
					'site' => $svc->getSite(),
					'err' => array_get(\PostgreSQL::pdo()->errorInfo(), '2', 'unknown error')
				]);
			}

			return true;
		}

		/**
		 * Before a domain may be deleted, its metrics must first be decompressed
		 */
		protected function checkForTelemetryCompression(): void
		{
			$pdo = \PostgreSQL::pdo();
			if (version_compare((new Connector($pdo))->getVersion(), '2.0', '>=')) {
				// v2 permits deletion of compressed records
				return;
			}
			$chunker = new Chunker($pdo);
			if (!$chunker->hasCompression()) {
				return;
			}

			info('Decompressing telemetry chunks. Hint: set [telemetry] => archival_compression to false to disable this requirement or run DeleteDomain in batch using opcenter.account-cleanup Scope.');
			$chunker->decompressAll();
		}
	}
