<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */


	namespace Opcenter\Service\Validators\Crontab;


	use Event\Cardinal;
	use Event\Events;
	use Opcenter\Provisioning\Ssh;
	use Opcenter\Service\Contracts\MountableLayer;
	use Opcenter\Service\Contracts\ServiceInstall;
	use Opcenter\Service\ServiceLayer;
	use Opcenter\SiteConfiguration;

	class Enabled extends \Opcenter\Service\Validators\Common\Enabled implements MountableLayer, ServiceInstall
	{
		public const DESCRIPTION = 'Task scheduling support';

		public function valid(&$value): bool
		{
			if ($value && SSH_CRONTAB_LINK && !$this->ctx->getServiceValue('ssh', 'enabled')) {
				return error('ssh service must be enabled to allow crontab');
			}
			if ($value && !$this->ctx['permit']) {
				warn('crontab service is enabled. Implicitly setting crontab,permit=1');
				$this->ctx['permit'] = 1;
			}
			return parent::valid($value);
		}

		public function populate(SiteConfiguration $svc): bool
		{
			if (!$this->ctx->getServiceValue('ssh', 'enabled')) {
				Cardinal::register([ServiceLayer::HOOK_ID, Events::END],
					static function () use ($svc) {
						Ssh::blockSsh($svc);
					}
				);
			}

			return true;
		}

		public function depopulate(SiteConfiguration $svc): bool
		{
			if ($this->ctx->getServiceValue('ssh', 'enabled') && !$this->ctx->isDelete()) {
				Cardinal::register([ServiceLayer::HOOK_ID, Events::END],
					static function () use ($svc) {
						Ssh::unblockSsh($svc);
					}
				);
			}
			return true;
		}

		public function getLayerName(): string
		{
			return 'ssh';
		}

		protected function manageLayer(int $value, string $layer): void
		{
			if (!$value && $this->ctx->getServiceValue('ssh', 'enabled')) {
				// avoid unmounting ssh layer when crontab is disabled
				return;
			}

			parent::manageLayer($value, $layer);
		}


	}
