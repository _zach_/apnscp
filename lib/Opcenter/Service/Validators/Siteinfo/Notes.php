<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, January 2019
	 */

	namespace Opcenter\Service\Validators\Siteinfo;

	use Opcenter\Service\ServiceValidator;

	class Notes extends ServiceValidator
	{
		const DESCRIPTION = 'Administrative account notes';

		public function valid(&$value): bool
		{
			if (!\is_array($value)) {
				$value = (array)$value;
			}
			return true;
		}

	}

