<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, August 2019
	 */


	namespace Opcenter\Service\Validators\Cgroup;

	use Opcenter\Service\ServiceValidator;

	class Ioweight extends ServiceValidator
	{
		const DESCRIPTION = 'Prioritize IO to process tasks (CFQ/BFQ only)';
		const VALUE_RANGE = '[10, 1000]';

		public function valid(&$value): bool
		{
			if (!$value || !$this->ctx->getServiceValue('cgroup', 'enabled')) {
				$value = null;

				return true;
			}

			// check if CFQ/BFQ and warn otherwise
			if (!\is_int($value)) {
				return error('%s weight must be an integer', $this->getWeightType());
			}

			if ($value > 1000) {
				return error('%s weight may not exceed 1000', $this->getWeightType());
			}

			if (static::class === self::class && $value < 10) {
				return error('%s weight must be at least 10', $this->getWeightType());
			}

			if ($value < 1) {
				return error('%s weight may not be less than 1', $this->getWeightType());
			}

			return true;
		}

		protected function getWeightType(): string
		{
			if (static::class === self::class) {
				return 'IO';
			}
			return strtoupper(substr(static::getBaseClassName(), 0, -6));
		}
	}
