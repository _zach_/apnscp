<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Service\Validators\Mail;

	use Opcenter\Service\Contracts\AlwaysValidate;
	use Opcenter\Service\Contracts\DefaultNullable;
	use Opcenter\Service\Contracts\ServiceReconfiguration;
	use Opcenter\Service\ServiceValidator;
	use Opcenter\Service\Validators\Common\GenericProvider;
	use Opcenter\SiteConfiguration;

	class Provider extends GenericProvider implements ServiceReconfiguration, AlwaysValidate, DefaultNullable
	{
		const DESCRIPTION = 'SMTP/IMAP/POP3 mail provider';

		public function valid(&$value): bool
		{

			if (!$this->ctx->getServiceValue($this->ctx->getModuleName(), 'enabled')) {
				$value = 'null';
				return true;
			}

			if ($value === DefaultNullable::NULLABLE_MARKER) {
				$value = $this->getDefault();
			}

			if ($value === null) {
				// -c mail,provider=null gets converted to literal type
				$value = 'null';
			}

			if (!\Opcenter\Mail::providerValid($value)) {
				return error("Unknown mail provider `%s'", $value);
			}

			return true;
		}

		public function getValidatorRange()
		{
			return \Opcenter\Mail::providers();
		}

		public function reconfigure($old, $new, SiteConfiguration $svc): bool
		{
			if ($old !== $new && $new === 'null' && $this->ctx['enabled']) {
				warn("All existing email records preserved for site. " .
					"Set mail,enabled=0 to also delete all email records.");
			}
			$this->freshenSite($svc);
			return true;
		}

		public function rollback($old, $new, SiteConfiguration $svc): bool
		{
			return true;
		}

		public function write()
		{
			$val = $this->ctx->getServiceValue(null, 'provider');

			return $val ?? 'null';
		}

		public function getDefault()
		{
			return MAIL_PROVIDER_DEFAULT ?? 'builtin';
		}


	}