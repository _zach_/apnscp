<?php
	declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, November 2020
	 */

	namespace Opcenter\Filesystem\Quota;

	use Opcenter\Filesystem\Mount;
	use Opcenter\Map\Textfile;
	use Opcenter\Reseller;

	class Project
	{
		const XFS_PROG = '/usr/sbin/xfs_quota';
		const FLAG_MAP = [
			'expert'   => 'x',
			'foreign'  => 'f',
			'projfile' => 'D',
			'projid'   => 'd'
		];
		const PROJID_MAP = '/etc/projid';
		const PROJECT_MAP = '/etc/projects';

		/**
		 * @var string base directory
		 */
		protected $base;

		/**
		 * Block device for path
		 *
		 * @var string|null
		 */
		private $mount;

		/**
		 * Project ID
		 *
		 * @var null
		 */
		private $projectId = null;

		/**
		 * Project constructor.
		 *
		 * @param string $base base directory
		 */
		public function __construct(string $base)
		{
			if (!static::supported()) {
				fatal("project quotas are not supported on this server configuration");
			}

			if ($base[0] !== '/') {
				$this->projectId = $this->projectId($base);
			} else {
				$this->projectId = $this->guessId($base);
			}
			$this->base = $base;
			$this->mount = Mount::getMount($base);
		}

		/**
		 * Project quotas supported on server
		 *
		 * @return bool
		 */
		public static function supported(): bool
		{
			static $enabled;
			if (isset($enabled)) {
				return $enabled;
			}
			return $enabled = (FILESYSTEM_TYPE === 'xfs' &&
				in_array('prjquota', (array)Mount::getMountOptions(FILESYSTEM_VIRTBASE), true));
		}

		/**
		 * Set project quota
		 * @param string|string[] $project
		 * @param int $bhard
		 * @param int $ihard
		 * @param int $bsoft
		 * @param int $isoft
		 * @return bool
		 */
		public static function setQuota($project, int $bhard = 0, int $ihard = 0, int $bsoft = 0, int $isoft = 0): bool
		{
			$ret = true;
			foreach ($project as $p) {
				$ret &= (new static($p))->set($bhard, $ihard, $bsoft, $isoft);
			}

			return $ret;
		}

		public static function getQuota($project): ?array
		{
			$ret = [];
			foreach ($project as $p) {
				 $ret[$p] = (new static($p))->get();
			}

			return is_array($project) ? $ret : array_pop($project);
		}

		public function get(): ?array
		{
			return [];
		}

		/**
		 * Set project quota
		 *
		 * @param int $bhard
		 * @param int $ihard
		 * @param int $bsoft
		 * @param int $isoft
		 * @return bool
		 */
		public function set(int $bhard = 0, int $ihard = 0, int $bsoft = 0, int $isoft = 0): bool
		{
			$ret = $this->exec('bhard=%d ihard=%d bsoft=%d isoft=%d', [$bhard, $ihard, $bsoft, $isoft]);
			return $ret['success'] ?: error($ret['stderr']);
		}

		/**
		 * Project quotas enabled for site
		 *
		 * @return bool
		 */
		public function enabled(): bool
		{
			return $this->guessId($this->base) !== null;
		}

		/**
		 * Enable project quotas for location
		 *
		 * @param string $name
		 * @return bool
		 */
		public function create(string $name): bool
		{
			$exec = $this->exec('project -s -p %s %d', [
				$this->base, $this->mapProject($name)
			]);
			return $exec['success'] ?: error($exec['stderr']);
		}

		/**
		 * Run command in xfs_progs
		 *
		 * @param string       $command
		 * @param array        $args    optional command arguments
		 * @param array|bool[] $flags   optional xfs_quota flags
		 * @return array
		 */
		private function exec(string $command, array $args = [], array $flags = ['expert' => true]): array
		{

			$cflags = [];
			foreach ($flags as $k => $v) {
				if (!isset(self::FLAG_MAP[$k])) {
					fatal("Unknown flag `%s'", $k);
				}
				$flag = self::FLAG_MAP[$k];
				if (is_bool($v)) {
					if (!$v) {
						continue;
					}
					$cflags[] = '-' . $flag;
				} else {
					$cflags[] = '-' . $flag . ' ' . escapeshellarg($v);
				}
			}

			$cmd = \ArgumentFormatter::format($command, array_map('escapeshellcmd', $args));
			return \Util_Process_Safe::exec(
				self::XFS_PROG . ' ' .
					implode(' ', $cflags) . ' -c \'' . escapeshellcmd($cmd) . '\' %s' ,
				$this->mount
			);
		}

		/**
		 * Map a project to a symbolic name
		 *
		 * @param string $name
		 * @return int
		 */
		public function mapProject(string $name): int
		{
			if (null !== ($id = $this->projectId($name))) {
				fatal("Project `%s' already exists with ID `%d'", $name, $id);
			}

			$map = new Textfile(self::PROJID_MAP, 'c', ':');
			$map->lock();

			$list = $map->fetchAll();
			$prev = 0;
			foreach ($list as $id) {
				if ($id !== $prev) {
					break;
				}
				$prev = $id;
			}
			$map[$name] = $id++;
			$map->save();

			return $id;
		}

		/**
		 * Remove quota support
		 *
		 * @return bool
		 */
		public function remove(): bool
		{
			$this->exec('limit -p bsoft=0 bhard=0 %s', [
				$this->getProject()
			]);

			return (bool)serial(function () {
				$this->exec('project -C %s', [$this->base]);

				$this->unmapProject($this->getProject());
				return true;
			});


		}

		/**
		 * Get project name
		 *
		 * @return string|null
		 */
		private function getProject(): ?string
		{
			$id = Reseller::idFromPath($this->base);
			if (null === $id) {
				return $id;
			}

			// make an informed decision
			$project = Reseller::RESELLER_PREFIX . $id;
			$chk = $this->projectId($project);
			if ($chk === $id) {
				return $project;
			}

			if ($chk !== $id && $chk) {
				warn("Check ID `%d' mismatch against `%s' - bug?", $chk, $project);
			}

			return null;
		}

		/**
		 * Unmap project
		 *
		 * @param string $name
		 * @return bool
		 */
		public function unmapProject(string $name): bool
		{
			$map = new Textfile(self::PROJID_MAP, 'c', ':');
			$map->lock();
			if (isset($map[$name])) {
				unset($map[$name]);
			} else {
				warn("Project `%(proj)s' not found in `%(path)s',",
					['proj' => $name, 'path' => self::PROJID_MAP]
				);
			}

			return $map->save();
		}

		/**
		 * Reassign directory new project
		 *
		 * @param string $owner
		 * @return bool
		 */
		public function reassign(string $owner): bool
		{
			fatal("Not implemented");
			return false;
		}

		/**
		 * Map a path to project
		 *
		 * @param string $id project ID
		 */
		private function setPathMap(int $id): void
		{
			$map = new Textfile(self::PROJID_MAP, 'r', ':');
			if (false === ($project = array_search($id, $map->fetchAll(), true))) {
				fatal("Project ID `%(projid)d' not found in %(path)s", [
					'projid' => $id, 'path' => self::PROJID_MAP
				]);
			}

			$map = new Textfile(self::PROJECT_MAP, 'c', ':');
			$map[$id] = $project;
			$map->save();
		}

		/**
		 * Lookup a project returning its identifier
		 *
		 * @param string $project
		 * @return int|null
		 */
		private function projectId(string $project): ?int
		{
			$map = new Textfile(self::PROJID_MAP, 'r', ':');
			return $map[$project] ?? null;
		}

		/**
		 * Lookup a project identifier returning its path
		 *
		 * @param int $id
		 * @return string|null
		 */
		private function idToPath(int $id): ?string
		{
			$map = new Textfile(self::PROJECT_MAP, 'r', ':');
			return $map[$id] ?? null;
		}

		/**
		 * Guess project ID from path
		 *
		 * @param string $path
		 * @return int|null
		 */
		private function guessId(string $path): ?int
		{
			if (null !== ($id = Reseller::idFromPath($path))) {
				$chk = $this->idToPath($id);
				if (0 === strpos($path . '/', $chk . '/')) {
					return $id;
				}
			}

			// expensive full scan
			$entries = (new Textfile(self::PROJECT_MAP, 'r', ':'))->fetchAll();

			$id = array_search($path, $entries, true);
			return $id !== false ? $id : null;
		}
	}