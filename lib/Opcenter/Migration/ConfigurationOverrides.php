<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, July 2019
 */

namespace Opcenter\Migration;

use Opcenter\SiteConfiguration;

class ConfigurationOverrides implements \ArrayAccess, \IteratorAggregate {

	private $cfg = [];

	/** @var SiteConfiguration */
	private $base;

	public function __construct(SiteConfiguration $s = null)
	{
		if ($s) {
			$this->cfg = $s->getNewConfiguration();
		}
	}

	public function setBaseConfiguration(SiteConfiguration $s) {
		$this->base = $s;
	}

	public function getIterator()
	{
		return new \ArrayIterator($this->cfg);
	}

	public function offsetExists($offset)
	{
		return array_has($this->cfg, $offset);
	}

	public function offsetGet($offset)
	{
		return array_get($this->cfg, $offset);
	}

	public function offsetSet($offset, $value)
	{
		array_set($this->cfg, $offset, $value);
	}

	public function offsetUnset($offset)
	{
		array_forget($this->cfg, $offset);
	}
}