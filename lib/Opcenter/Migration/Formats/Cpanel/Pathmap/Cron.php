<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, August 2019
 */

	namespace Opcenter\Migration\Formats\Cpanel\Pathmap;

	use Event\Cardinal;
	use Event\Events;
	use Opcenter\Filesystem;
	use Opcenter\Migration\Formats\Cpanel\ConfigurationBuilder;
	use Opcenter\Migration\Notes\User;
	use Opcenter\SiteConfiguration;

	class Cron extends ConfigurationBuilder
	{
		const DEPENDENCY_MAP = [
			Homedir::class,
			Shell::class
		];
		public function parse(): bool
		{
			if (!$path = $this->getDirectoryEnumerator()->getPathname()) {
				return true;
			}

			if (!array_get($this->task->getConfigurationOverrides(), 'crontab.enabled', true)) {
				return warn('Crontab override set. Disabling importation of crontab tasks');
			} else if (!$this->bill->get('crontab', 'enabled', true)) {
				warn('Crontab detected. Enabling crond usage');
			}

			$this->bill->set('crontab', 'enabled', true);
			if (!$this->bill->get('ssh', 'enabled', true)) {
				// It's possible to launch a long-running tunnel or any daemon via
				// cron to allow SSH into the account. Security through obscurity is dumb.
				warn('Crontab usage enabled on account. Crontab implicitly enables SSH.');
			}
			$this->bill->set('ssh', 'enabled', true);

			// callback once account is processed
			Cardinal::register(
				[\Opcenter\Account\Import::HOOK_ID, Events::SUCCESS],
				function ($event, SiteConfiguration $s) use ($path) {
					$cron = preg_replace('!^\s*SHELL=("?)/usr/local/cpanel/bin/jailshell\1$!m', '',
						file_get_contents($path));

					if (!$s->getSiteFunctionInterceptor()->crontab_enabled() && !$s->getSiteFunctionInterceptor()->crontab_toggle_status(1)) {
						warn('Failed to enable crontab');
					}
					$ctx = $s->getAuthContext();
					$spoolPath = $ctx->domain_fs_path(\Crontab_Module::CRON_SPOOL . '/' . (string)$this->bill->getNote(User::class));
					// do a straight copy until crontab:add-raw is implemented
					file_put_contents($spoolPath, $cron) && Filesystem::chogp($spoolPath, $ctx->user_id, $ctx->group_id,
						0600);
					$s->getSiteFunctionInterceptor()->crontab_reload();
				}
			);

			return true;
		}
	}

