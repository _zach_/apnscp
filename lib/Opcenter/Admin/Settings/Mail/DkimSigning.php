<?php declare(strict_types=1);

	namespace Opcenter\Admin\Settings\Mail;

	use Opcenter\Admin\Bootstrapper;
	use Opcenter\Admin\Bootstrapper\Config;
	use Opcenter\Admin\Settings\SettingsInterface;
	use Opcenter\Mail\Services\Rspamd;

	class DkimSigning implements SettingsInterface
	{
		public function set($val): bool
		{
			if ($val == $this->get()) {
				// no need to set the value again
				return true;
			}

			if (!Rspamd::present()) {
				return error("rspamd must be enabled");
			}

			$config = new Config();
			$config['rspamd_dkim_signing'] = (bool)$val;

			unset($config);
			Bootstrapper::run('mail/rspamd');
			if ($val) {
				// must run after as mail/rspamd populates global key
				warn("Run dkim:roll after Bootstrapper completes");
			} else {
				info("Run dkim:expire '%s' to expire old DKIM DNS records",
					Rspamd\Dkim::instantiateContexted(\Auth::profile())->selector()
				);
			}
			return true;
		}

		public function get()
		{
			$cfg = new Config();

			return $cfg['rspamd_dkim_signing'] && Rspamd::present();
		}

		public function getHelp(): string
		{
			return 'Enable rspamd DKIM signing';
		}

		public function getValues()
		{
			return 'bool';
		}

		public function getDefault()
		{
			return false;
		}
	}
