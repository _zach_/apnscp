<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, August 2018
	 */

	namespace Opcenter\Admin\Settings\Apache;

	use Opcenter\Http\Apache;
	use Opcenter\Map;

	class Cachetype extends SystemDirective
	{
		public function set($val, ...$x): bool
		{
			if ($val && $val !== 'implicit' && $val !== 'explicit') {
				return error("unknown setting `%s', valid settings: %s",
					$val,
					implode(',', $this->getValues())
				);
			}
			if ($val == $this->get()) {
				return true;
			}

			if (!$val) {
				return parent::set('NO_CACHE', true);
			}

			$contents = file_get_contents(static::CONFIG_FILE);
			if ($val === 'explicit') {
				$contents = preg_replace('/^\s*unsetenv no-cache\s*$[\r\n]{0,2}/im', '', $contents);
			} else {
				$contents = rtrim($contents) . "\nUnsetEnv no-cache\n";
			}

			return file_put_contents(static::CONFIG_FILE, $contents, LOCK_EX) > 0 && Apache::restart();

		}

		public function getValues()
		{
			return ['implicit', 'explicit'];
		}

		public function get(...$directive)
		{
			$ini = Map::load(Apache::SYSCONFIG_FILE);
			if (!($opts = $ini->section(null)->quoted(true)->offsetGet('OPTIONS'))) {
				return true;
			}

			if (false !== strpos($opts, '-DNO_CACHE')) {
				return null;
			}

			return false === stripos(file_get_contents(static::CONFIG_FILE),
				'UnsetEnv no-cache') ? 'explicit' : 'implicit';
		}

		public function getHelp(): string
		{
			return 'Change upstream HTTP caching strategy';
		}

		public function getDefault()
		{
			return 'explicit';
		}
	}