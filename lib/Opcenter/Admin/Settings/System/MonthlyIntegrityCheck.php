<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2019
	 */

	namespace Opcenter\Admin\Settings\System;

	use Opcenter\Admin\Bootstrapper;
	use Opcenter\Admin\Settings\SettingsInterface;

	class MonthlyIntegrityCheck implements SettingsInterface
	{
		public function set($val): bool
		{
			if ($val == $this->get()) {
				return true;
			}

			$cfg = new Bootstrapper\Config();
			$cfg['monthly_platform_check'] = (bool)$val;
			unset($cfg);
			Bootstrapper::run('apnscp/crons');

			return true;
		}

		public function get()
		{
			return (new Bootstrapper\Config())['monthly_platform_check'];
		}

		public function getHelp(): string
		{
			return 'Perform a full platform integrity check every month';
		}

		public function getValues()
		{
			return 'bool';
		}

		public function getDefault()
		{
			return true;
		}
	}