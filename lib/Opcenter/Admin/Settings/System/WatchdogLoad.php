<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, August 2018
	 */

	namespace Opcenter\Admin\Settings\System;

	use Opcenter\Admin\Bootstrapper;
	use Opcenter\Admin\Bootstrapper\Config;
	use Opcenter\Admin\Settings\SettingsInterface;
	use Opcenter\Map;

	class WatchdogLoad implements SettingsInterface
	{
		public const WATCHDOG_CONF = '/etc/watchdog.conf';

		public function set($val): bool
		{
			if (!\is_int($val) || $val < 0) {
				return error('Watchdog value must be a positive integer');
			}

			if ($val === $this->get()) {
				return true;
			}

			$cfg = new Config();
			$cfg['watchdog_load_threshold'] = $val;
			unset($cfg);
			Bootstrapper::run('software/watchdog');

			return true;
		}

		public function get()
		{
			if (!file_exists(self::WATCHDOG_CONF)) {
				return null;
			}
			$map = Map::load(self::WATCHDOG_CONF, 'r', 'inifile');

			return $map['max-load-1'] ?? null;
		}

		public function getHelp(): string
		{
			return 'Set watchdog load threshold';
		}

		public function getValues()
		{
			return 'int > 0';
		}

		public function getDefault()
		{
			return 25 * NPROC;
		}
	}