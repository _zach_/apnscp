<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, February 2019
	 */

	namespace Opcenter\Admin\Settings\VirusScanner;

	use Opcenter\Admin\Bootstrapper;
	use Opcenter\Admin\Settings\SettingsInterface;

	class Enabled implements SettingsInterface
	{
		const SCANNERS = ['clamav'];
		const DEFAULT_SCANNER = 'clamav';

		public function set($val): bool
		{
			if ($val === true) {
				$val = self::DEFAULT_SCANNER;
			}
			if ($val === $this->get()) {
				return true;
			}
			if ($val && !\in_array($val, self::SCANNERS, true)) {
				return error("Unknown AV scanner `%s'", $val);
			}
			$cfg = new Bootstrapper\Config();
			if ($val) {
				$cfg[$val . '_enabled'] = (bool)$val;
			} else {
				foreach (self::SCANNERS as $scanner) {
					$cfg[$scanner . '_enabled'] = false;
				}
			}
			$cfg->sync();
			Bootstrapper::run('clamav/setup', 'apache/modsecurity');

			return true;
		}

		public function get()
		{
			return ANTIVIRUS_INSTALLED === true ? self::DEFAULT_SCANNER : ANTIVIRUS_INSTALLED;
		}

		public function getHelp(): string
		{
			return 'Set system AV scanner';
		}

		public function getValues()
		{
			return 'bool';
		}

		public function getDefault()
		{
			return true;
		}
	}