<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, September 2018
	 */

	namespace Opcenter\Admin\Settings\Rampart;

	use Opcenter\Admin\Bootstrapper;
	use Opcenter\Admin\Settings\SettingsInterface;
	use Opcenter\Map;

	class Fail2banWhitelist implements SettingsInterface
	{
		public const FAIL2BAN_CONF = '/etc/fail2ban/jail.conf';

		public function set($val = ''): bool
		{
			if (!$val) {
				$val = \Auth::client_ip();
			}

			if (\in_array($val, $this->get())) {
				return true;
			}

			Bootstrapper::run('fail2ban/whitelist-self', ['whitelist_ip' => $val, 'whitelist_mode' => 'append']);

			return true;
		}

		public function get()
		{
			if (!file_exists(self::FAIL2BAN_CONF)) {
				return null;
			}
			$map = Map::load(self::FAIL2BAN_CONF, 'r', 'inifile')->section('DEFAULT')->offsetGet('ignoreip');

			return preg_split('/\s+/', (string)$map, -1, PREG_SPLIT_NO_EMPTY);
		}

		public function getHelp(): string
		{
			return 'Whitelist an IP address from Rampart';
		}

		public function getValues()
		{
			return 'string';
		}

		public function getDefault()
		{
			return '127.0.0.1/8';
		}
	}