<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	$__majordomo_preamble = <<<EOT
# The configuration file for a majordomo mailing list.
# Comments start with the first # on a line, and continue to the end
# of the line. There is no way to escape the # character. The file
# uses either a key = value for simple (i.e. a single) values, or uses
# a here document
#     key << END
#     value 1
#     value 2
#     [ more values 1 per line]
#     END
# for installing multiple values in array types. Note that the here
# document delimiter (END in the example above) must be the same at the end
# of the list of entries as it is after the << characters.
# Within a here document, the # sign is NOT a comment character.
# A blank line is allowed only as the last line in the here document.
#
# The values can have multiple forms:
#
#       absolute_dir -- A root anchored (i.e begins with a /) directory
#       absolute_file -- A root anchored (i.e begins with a /) file
#       bool -- choose from: yes, no, y, n
#       enum -- One of a list of possible values
#       integer -- an integer (string made up of the digits 0-9,
#                  no decimal point)
#       float -- a floating point number with decimal point.
#       regexp -- A perl style regular expression with
#                 leading and trailing /'s.
#       restrict_post -- a series of space or : separated file names in which
#                        to look up the senders address
#                   (restrict-post should go away to be replaced by an
#                    array of files)
#       string -- any text up until a \\n stripped of
#                 leading and trailing whitespace
#       word -- any text with no embedded whitespace
#
# A blank value is also accepted, and will undefine the corresponding keyword.
# The character Control-A may not be used in the file.
#
# A trailing _array on any of the above types means that that keyword
# will allow more than one value.
#
# Within a here document for a string_array, the '-' sign takes on a special
# significance.
#
#     To embed a blank line in the here document, put a '-' as the first
#       and ONLY character on the line.
#
#     To preserve whitespace at the beginning of a line, put a - on the
#       line before the whitespace to be preserved
#
#     To put a literal '-' at the beginning of a line, double it.
#
#
# The default if the keyword is not supplied is given in ()'s while the
# type of value is given in [], the subsystem the keyword is used in is
# listed in <>'s. (undef) as default value means that the keyword is not
# defined or used.
\n
EOT;

	$__majordomo_skeleton = array(
		'admin_passwd'  => array(
			'type' => string,
			'help' => 'The password for handling administrative tasks on the list.',
			'opt'  => OPT_REQUIRED
		)
	,
		'administrivia' => array(
			'type'    => bool,
			'help'    => 'Look for administrative requests (e.g. subscribe/unsubscribe) and forward them to the list maintainer instead of the list.',
			'opt'     => OPT_REQUIRED,
			'default' => true
		)
	,

		'advertise'      => array(
			'type'    => text,
			'help'    => 'If the requestor email address matches one of these regexps, then the list will be listed in the output of a lists command. Failure to match any regexp excludes the list from the output. The regexps under noadvertise override these regexps.',
			'default' => ''
		)
	,
		'announcements'  => array(
			'type'    => bool,
			'default' => true,
			'help'    => 'If set to yes, comings and goings to the list will be sent to the list owner. These SUBSCRIBE/UNSUBSCRIBE event announcements are informational only (no action is required), although it is highly recommended that they be monitored to watch for list abuse.'
		)
	,
		'approve_passwd' => array(
			'type' => string,
			'help' => 'Password to be used in the approved header to allow posting to moderated list, or to bypass resend checks.',
			'opt'  => OPT_REQUIRED
		)
	,
		'archive_dir'    => array(
			'type' => string,
			'help' => 'The directory where the mailing list archive is kept. This item does not currently work. Leave it blank.'
		)
	,
		'comments'       => array(
			'type' => text,
			'help' => 'Comment string that will be retained across config file rewrites.'
		)
	,
		'date_info'      => array(
			'type'    => bool,
			'default' => true,
			'help'    => 'Put the last updated date for the info file at the top of the ' .
				'info file rather than having it appended with an info command.  ' .
				'This is useful if the file is being looked at by some means other ' .
				'than majordomo (e.g. finger).'
		)
	,
		# date_intro           [bool] (yes) <majordomo>

		'date_intro' => array(
			'type'    => bool,
			'default' => true,
			'help'    => 'Put the last updated date for the intro file at the top of the ' .
				'intro file rather than having it appended with an intro command.  ' .
				'This is useful if the file is being looked at by some means other ' .
				'than majordomo (e.g. finger).'
		)
	,
		'debug'      => array(
			'type'    => bool,
			'default' => false,
			'help'    => 'Don\'t actually forward message, just go though the motions.'
		)
	,
		# description          [string] (undef) <majordomo>

		'description'    => array(
			'type'    => string,
			'default' => '',
			'help'    => 'Used as description for mailing list when replying to the lists ' .
				'command. There is no quoting mechanism, and there is only room ' .
				'for 50 or so characters.'
		)
	,
		# digest_archive       [absolute_dir] (undef) <digest>
		#
		'digest_archive' => array(
			'type'    => string,
			'default' => '',
			'help'    => 'The directory where the digest archive is kept. This item does ' .
				'not currently work. Leave it blank.'
		)
	,
		# digest_issue         [integer] (1) <digest>
		#
		'digest_issue'   => array(
			'type'    => integer,
			'default' => 1,
			'help'    => 'The issue number of the next issue'
		)
	,
		# digest_maxdays       [integer] (undef) <digest>

		'digest_maxdays' => array(
			'type'    => integer,
			'default' => null,
			'help'    => 'Automatically generate a new digest when the age of the oldest article in the queue exceeds this number of days.'
		)
	,
		# digest_maxlines      [integer] (undef) <digest>

		'digest_maxlines' => array(
			'type'    => integer,
			'default' => null,
			'help'    => 'Automatically generate a new digest when the size of the digest exceeds this number of lines.'
		)
	,
		# digest_name          [string] (layout) <digest>

		'digest_name'       => array(
			'type'    => string,
			'default' => 'layout',
			'help'    => 'The subject line for the digest. This string has the volume and issue appended to it.'
		)
	,
		# digest_rm_footer     [word] (undef) <digest>
		#
		'digest_rm_footer'  => array(
			'type' => string,
			'help' => 'The value is the name of the list that applies the header and ' .
				'footers to the messages that are received by digest. This allows ' .
				'the list supplied headers and footers to be stripped before the ' .
				'messages are included in the digest.'
		)
	,
		# digest_rm_fronter    [word] (undef) <digest>
		#
		'digest_rm_fronter' => array(
			'type' => string,
			'help' => 'Works just like digest_rm_footer, except it removes the front material.'
		)
	,
		# digest_volume        [integer] (1) <digest>
		'digest_volume'     => array(
			'type'    => integer,
			'default' => 1,
			'help'    => 'The current volume number.'
		)
	,
		# digest_work_dir      [absolute_dir] (undef) <digest>

		'digest_work_dir' => array(
			'type'    => string,
			'default' => '',
			'help'    => 'The directory used as scratch space for digest. Don\'t  change ' .
				'this unless you know what you are doing'
		)
	,
		# get_access           [enum] (list) <majordomo> /open;closed;list/

		'get_access'   => array(
			'type'    => enum,
			'values'  => array('open', 'closed', 'list'),
			'default' => 'list',
			'help'    => 'One of three values: open, list, closed. Open allows anyone ' .
				'access to this command and closed completely disables the command ' .
				'for everyone. List allows only list members access, or if ' .
				'restrict_post is defined, only the addresses in those files are ' .
				'allowed access.'
		)
	,
		# index_access         [enum] (open) <majordomo> /open;closed;list/
		#
		'index_access' => array(
			'type'    => enum,
			'values'  => array('open', 'closed', 'list'),
			'default' => 'open',
			'help'    => 'One of three values: open, list, closed. Open allows anyone ' .
				'access to this command and closed completely disables the command ' .
				'for everyone. List allows only list members access, or if ' .
				'restrict_post is defined, only the addresses in those files are ' .
				'allowed access.'
		)

	,
		# info_access          [enum] (open) <majordomo> /open;closed;list/

		'info_access'  => array(
			'type'    => enum,
			'default' => 'open',
			'values'  => array('open', 'closed', 'list'),
			'help'    => 'One of three values: open, list, closed. Open allows anyone ' .
				'access to this command and closed completely disables the command ' .
				'for everyone. List allows only list members access, or if ' .
				'restrict_post is defined, only the addresses in those files are ' .
				'allowed access.'
		)
	,
		# intro_access         [enum] (list) <majordomo> /open;closed;list/
		#
		'intro_access' => array(
			'type'    => enum,
			'values'  => array('open', 'closed', 'list'),
			'default' => 'list',
			'help'    => 'One of three values: open, list, closed. Open allows anyone ' .
				'access to this command and closed completely disables the command ' .
				'for everyone. List allows only list members access, or if ' .
				'restrict_post is defined, only the addresses in those files are /var/lib/majordomo/lists/emt ' .
				'allowed access.'
		)
	,
		# maxlength            [integer] (10000000000000000) <resend,digest>

		'maxlength' => array(
			'type'    => integer,
			'default' => 10000000,
			'help'    => 'The maximum size of an unapproved message in characters. When ' .
				'used with digest, a new digest will be automatically generated if ' .
				'the size of the digest exceeds this number of characters.  Setting this ' .
				'value above 10,000,000 is not advisable.'
		)
	,
		# message_footer       [string_array] (undef) <resend,digest>

		'message_footer'  => array(
			'type'    => text,
			'default' => '',
			'help'    => 'Text to be appended at the end of all messages posted to the ' .
				'list. The text is expanded before being used. The following ' .
				'expansion tokens are defined: $LIST - the name of the current ' .
				'list, $SENDER - the sender as taken from the from line, $VERSION, ' .
				'the version of majordomo. If used in a digest, no expansion ' .
				'tokens are provided'
		)
	,
		# message_fronter      [string_array] (undef) <resend,digest>
		#
		'message_fronter' => array(
			'type'    => text,
			'default' => '',
			'help'    => 'Text to be prepended to the beginning of all messages posted to ' .
				'the list. The text is expanded before being used. The following ' .
				'expansion tokens are defined: $LIST - the name of the current ' .
				'list, $SENDER - the sender as taken from the from line, $VERSION, ' .
				'the version of majordomo. If used in a digest, only the expansion ' .
				'token _SUBJECTS_ is available, and it expands to the list of ' .
				'message subjects in the digest'
		)
	,
		# message_headers      [string_array] (undef) <resend,digest>
		#
		'message_headers' => array(
			'type'    => text,
			'default' => '',
			'help'    => 'These headers will be appended to the headers of the posted ' .
				'message. The text is expanded before being used. The following ' .
				'expansion tokens are defined: $LIST - the name of the current ' .
				'list, $SENDER - the sender as taken from the from line, $VERSION, ' .
				'the version of majordomo.'
		)
	,
		# moderate             [bool] (no) <resend>

		'moderate'       => array(
			'type'    => bool,
			'default' => false,
			'help'    => 'If yes, all postings to the list will be bounced to the moderator for approval.'
		)
	,
		# moderator            [word] (undef) <resend>
		#
		'moderator'      => array(
			'type'    => string,
			'default' => '',
			'help'    => 'Address for directing posts which require approval. Such ' .
				'approvals might include moderated mail, administrivia traps, and ' .
				'restrict_post authorizations. If the moderator address is not ' .
				'set, it will default to the list-approval address.'
		)
	,
		# mungedomain          [bool] (no) <majordomo>
		#
		'mungedomain'    => array(
			'type'    => bool,
			'default' => false,
			'help'    => 'If set to yes, a different method is used to determine a matching ' .
				'address.  When set to yes, addresses of the form user@dom.ain.com ' .
				'are considered equivalent to addresses of the form user@domain.com. ' .
				'This allows a user to subscribe to a list using the domain ' .
				'address rather than the address assigned to a particular machine ' .
				'in the domain. This keyword affects the interpretation of ' .
				'addresses for subscribe, unsubscribe, and all private options.'
		)
	,
		# noadvertise          [regexp_array] (undef) <majordomo>
		#
		'noadvertise'    => array(
			'type'    => text,
			'default' => '',
			'help'    => 'Regex: If the requestor name matches one of these regexps, then the list ' .
				'will not be listed in the output of a lists command. Noadvertise ' .
				'overrides advertise.'
		)
	,
		# precedence           [word] (bulk) <resend,digest>
		#
		'precedence'     => array(
			'type'    => string,
			'default' => 'bulk',
			'help'    => 'Put a precedence header with value <value> into the outgoing message.'
		)
	,
		# purge_received       [bool] (no) <resend>
		#
		'purge_received' => array(
			'type'    => bool,
			'default' => false,
			'help'    => 'Remove all received lines before resending the message.'
		)
	,
		# reply_to             [word] () <resend,digest>

		'reply_to' => array(
			'type'    => string,
			'default' => '',
			'help'    => 'Put a reply-to header with value <value> into the outgoing ' .
				'message. If the token $SENDER is used, then the address of the ' .
				'sender is used as the value of the reply-to header. This is the ' .
				'value of the reply-to header for digest lists.'
		)
	,
		# resend_dmarc         [bool] (yes) <resend>

		'resend_dmarc' => array(
			'type'    => bool,
			'default' => false,
			'help'    => 'Set to yes if you want this list to rewrite the From address to the ' .
				'mailing list address. This is necessary to avoid DMARC violations.'
		)
	,
		# resend_host          [word] (undef) <resend>

		'resend_host' => array(
			'type'    => string,
			'default' => '',
			'help'    => 'The host name that is appended to all address strings specified for resend.'
		)
	,
		# restrict_post        [restrict_post] (undef) <resend> /var/lib/majordomo/lists/emt

		'restrict_post' => array(
			'default' => '',
			'type'    => string,
			'help'    => 'If defined, only addresses listed in these files (colon or space ' .
				'separated) can post to the mailing list. By default, these files ' .
				'are relative to the lists directory. These files are also checked ' .
				'when get_access, index_access, info_access, intro_access, ' .
				'which_access, or who_access is set to \'list\'. This is less useful ' .
				'than it seems it should be since there is no way to create these ' .
				'files if you do not have access to the machine running resend. ' .
				'This mechanism will be replaced in a future version of ' .
				'majordomo/resend.'
		)
	,
		# sender               [word] (owner-layout) <majordomo,resend,digest

		'sender' => array(
			'type'    => string,
			'default' => '',
			'help'    => 'The envelope and sender address for the resent mail. This string ' .
				'has "@" and the value of resend_host appended to it to make a ' .
				'complete address. For majordomo, it provides the sender address ' .
				'for the welcome mail message generated as part of the subscribe ' .
				'command.'
		)
	,
		# strip                [bool] (yes) <majordomo>

		'strip' => array(
			'type'    => bool,
			'default' => true,
			'help'    => 'When adding address to the list, strip off all comments etc, and ' .
				'put just the raw address in the list file.  In addition to the ' .
				'keyword, if the file <listname>.strip exists, it is the same as ' .
				'specifying a yes value. That yes value is overridden by the value ' .
				'of this keyword.'
		)
	,
		# subject_prefix       [word] (undef) <resend>

		'subject_prefix' => array(
			'type'    => string,
			'default' => '',
			'help'    => 'This word will be prefixed to the subject line, if it is not ' .
				'already in the subject. The text is expanded before being used. ' .
				'The following expansion tokens are defined: $LIST - the name of ' .
				'the current list, $SENDER - the sender as taken from the from ' .
				'line, $VERSION, the version of majordomo.'
		)
	,
		# subscribe_policy     [enum] (open+confirm) <majordomo> /open;closed

		'subscribe_policy'   => array(
			'type'    => enum,
			'values'  => array('open', 'closed', 'auto', 'open+confirm', 'closed+confirm', 'auto+confirm'),
			'default' => 'open+confirm',
			'help'    => 'One of three values: open, closed, auto; plus an optional modifier: \'+confirm\'. ' .
				'Open allows people to subscribe themselves to the list. Auto allows anybody to ' .
				'subscribe anybody to the list without maintainer approval. Closed requires maintainer ' .
				'approval for all subscribe requests to the list.  Adding \'+confirm\', ie, \'open+confirm\', ' .
				'will cause majordomo to send a reply back to the subscriber which includes a authentication ' .
				'number which must be sent back in with another subscribe command.'
		)
	,
		'taboo_body'         => array(
			'type'    => text,
			'default' => '',
			'help'    => 'If any line of the body matches one of these regular expressions (e.g. /[a-z]+/), then the message will be bounced for review.'
		)
	,
		'taboo_headers'      => array(
			'type'    => text,
			'default' => '',
			'help'    => 'If any of the headers matches one of these regular expressions (e.g. /[a-z]+/), then the message will be bounced for review.'
		)
	,
		'unsubscribe_policy' => array(
			'type'    => enum,
			'values'  => array('open', 'closed', 'auto', 'auto+confirm', 'op'),
			'default' => 'open',
			'help'    => 'One of three values: open, closed, auto; plus an optional modifier: \'+confirm\'. ' .
				'Open allows people to unsubscribe themselves from the list. Auto allows anybody to ' .
				'unsubscribe anybody to the list without maintainer approval. The existence of ' .
				'the file <listname>.auto is the same as specifying the value auto.  Closed requires ' .
				'maintainer approval for all unsubscribe requests to the list. In addition to the ' .
				'keyword, if the file <listname>.closed exists, it is the same as specifying the value ' .
				'closed. Adding \'+confirm\', ie, \'auto+confirm\', will cause majordomo to send a reply ' .
				'back to the subscriber if the request didn\'t come from the subscriber. The reply includes ' .
				'a authentication number which must be sent back in with another subscribe command.  ' .
				'The value of this keyword overrides the value supplied by any existent files.'
		)
	,
		'welcome'            => array(
			'type'    => bool,
			'default' => true,
			'help'    => 'If set to yes, a welcome message (and optional \'intro\' file) will be sent to the newly subscribed user.'
		)
	,
		'which_access'       => array(
			'type'    => enum,
			'values'  => array('open', 'closed', 'list'),
			'default' => 'open',
			'help'    => 'One of three values: open, list, closed. Open allows anyone access to this ' .
				'command and closed completely disables the command for everyone. List ' .
				'allows only list members access, or if restrict_post is defined, only ' .
				'the addresses in those files are allowed access.'
		)
	,
		'who_access'         => array(
			'type'    => enum,
			'help'    => 'One of three values: open, list, closed. Open allows anyone access to this ' .
				'command and closed completely disables the command for everyone. List allows ' .
				'only list members access, or if restrict_post is defined, only the addresses ' .
				'in those files are allowed access.',
			'default' => 'closed',
			'values'  => array('open', 'closed', 'list')
		)
	);