<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, April 2020
 */

namespace Module\Support\Webapps;

use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use HTTP\SelfReferential;

class Assurance
{
	use \ContextableTrait;
	use \apnscpFunctionInterceptorTrait;

	/**
	 * Request suceeded
	 *
	 * @var bool
	 */
	protected $succeeded = false;
	/**
	 * @var string
	 */
	protected $hostname;
	/**
	 * @var string
	 */
	protected $path;
	/**
	 * @var int original body size
	 */
	protected $originalLength = 0;
	/**
	 * @var Git
	 */
	protected $git;
	/**
	 * @var string|null
	 */
	protected $originalCommit;

	protected function __construct(UpdateCandidate $candidate, Git $git)
	{
		$this->hostname = $candidate->getHostname();
		$this->path     = $candidate->getPath();
		$this->git = $git;
		$this->originalCommit = $this->snapshot();
		$this->record();
	}

	public function __destruct()
	{
		// meld old commits ?
	}

	/**
	 * Record initial size
	 */
	protected function record(): void
	{
		$size = $this->measure();
		$this->succeeded = $size !== null;
		$this->originalLength = $size;
	}

	/**
	 * Rollback snapshot
	 *
	 * @param string|null $commit commit to rollback to
	 * @return bool
	 */
	public function rollback(string $commit = null): bool {
		return $this->git->restore($commit ?? $this->originalCommit, true) ?
			warn('Restored previous snapshot') :
			false;
	}

	/**
	 * Take a snapshot prior to update
	 *
	 * @return string
	 */
	public function snapshot(): ?string {
		return $this->git->snapshot(_('Automatic update'));
	}

	/**
	 * Get page size
	 *
	 * @return int|null
	 */
	public function measure(int $retry = 0): ?int
	{
		$hostname = $this->hostname;
		if (false === strpos($hostname, '.')) {
			// global subdomain
			$hostname .= '.' . $this->getAuthContext()->domain;
		}
		$req = new SelfReferential($hostname, $this->getApnscpFunctionInterceptor()->site_ip_address());
		try {
			$contents = $req->get($this->path, ['pragma' => 'no-cache', 'cache-control' => 'no-cache']);
		} catch (\InvalidArgumentException $ex) {
			error('Update Assurance failure: %s', $ex->getMessage());
			return null;
		} catch (ClientException|ServerException $ex) {
			if ($this->originalLength) {
				if (!$retry && $ex->getCode() === 503) {
					// Ghost...
					sleep(10);
					return $this->measure(++$retry);
				}
				error('Update Assurance failure: bad HTTP response from server: %d', $ex->getResponse()->getStatusCode());
			} else {
				warn('Update Assurance initial check failed: %s', $ex->getMessage());
			}
			// bad status
			return null;
		}

		return \strlen($contents->getBody()->getContents());
	}

	/**
	 * Get checkpoint size
	 *
	 * @return int
	 */
	public function getInitialLength(): int
	{
		return $this->originalLength;
	}

	/**
	 * Page within last observed value
	 *
	 * @return bool
	 */
	public function assert(): bool
	{
		if (!$this->succeeded) {
			// failed on initial check
			return true;
		}

		if (null === ($newSize = $this->measure())) {
			return false;
		}

		$drift = abs(($newSize - $this->getInitialLength()) / $this->getInitialLength());
		return $drift <= WEBAPPS_ASSURANCE_DRIFT ?: error('Update Assurance failure: page changed by %.2f%%', $drift*100);
	}

	/**
	 * Get tracking tag
	 *
	 * @return string
	 */
	public function getTag(): string {
		return $this->getAuthContext()->site . ':' . $this->hostname . $this->path;
	}

}

