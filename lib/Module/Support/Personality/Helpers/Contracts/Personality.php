<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, June 2017
	 */

	namespace Module\Support\Personality\Helpers\Contracts;

	interface Personality
	{
		public function getPriority();

		public function resolves($token);

		public function getTokenDescription($token);

		public function personalityName();

		public function getDirectives();

		public function test($directive, $val = null);


	}