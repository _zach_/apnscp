<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, May 2020
 */


namespace Util\Process\Unshared\Resources;

use Util\Process\Unshared\Contracts\Unshareable;

class Mount implements Unshareable {
	const PROPAGATION_MODES = [
		'private',
		'shared',
		'slave',
		'unchanged'
	];

	/**
	 * @var string
	 */
	protected $path;
	/**
	 * @var resource|string
	 */
	protected $src;
	protected $cleanup;
	// @var string propagation mode
	protected $propagation = 'private';

	public function __construct(string $path, bool $create = false)
	{
		if ($path[0] !== '/') {
			fatal('Path %s must be absolute', $path);
		}

		if (!$create && !file_exists($path)) {
			fatal("Cannot unshare non-existent path `%s'", $path);
		}

		$this->path = $path;
		if (!$create) {
			return;
		}

		if (file_exists($this->path)) {
			warn("File `%s' already exists", $this->path);
			return;
		}
		touch($this->path);
		$this->cleanup = $this->path;
	}

	public function __destruct()
	{
		if ($this->cleanup) {
			file_exists($this->cleanup) && unlink($this->cleanup);
		}
	}

	public function fromResource($fp): self
	{
		$this->src = $fp;

		return $this;
	}

	/**
	 * Set
	 * @param string $file
	 * @return $this
	 */
	public function fromFile(string $file): self {
		$this->src = $file;
		return $this;
	}

	public function fromContent(string $content, string $owner = null): self
	{
		$this->src = tmpfile();
		$path = stream_get_meta_data($this->src)['uri'];
		if ($owner && 0 === posix_getuid()) {
			chown($path, $owner) || warn('Failed to chown %(file)s to %(user)s', ['file' => $path, 'user' => $owner]);
		}
		chmod($path, fileperms($this->path));
		fwrite($this->src, $content);

		return $this;
	}

	public function getInitializer(): ?string
	{
		return '--propagation ' . $this->propagation;
	}

	/**
	 * Get backed file
	 *
	 * @return string
	 */
	protected function getFile(): string
	{
		if (\is_resource($this->src)) {
			return stream_get_meta_data($this->src)['uri'];
		}

		return $this->src;
	}

	/**
	 * Set mount propagation mode
	 *
	 * @param string $mode
	 * @return bool
	 */
	public function setPropagation(string $mode): self
	{
		if (!\in_array($mode, self::PROPAGATION_MODES, true)) {
			fatal("Unknown propagation mode `%s'", $mode);
		}

		$this->propagation = $mode;

		return $this;
	}

	public function __toString(): string
	{
		$srcPath = $this->getFile();

		return 'mount ' . escapeshellarg($srcPath) . ' ' . escapeshellarg($this->path) . ' --bind';
	}
}