<?php

	namespace Lararia\Mail;

	use Illuminate\Bus\Queueable;
	use Illuminate\Mail\Mailable;
	use Illuminate\Queue\SerializesModels;
	use Lararia\Jobs\Job\Report;

	class JobFinished extends Mailable
	{
		use Queueable, SerializesModels;


		protected $report;

		/**
		 * Create a new message instance.
		 *
		 * @param Report $job
		 * @param array  $log
		 */
		public function __construct(Report $job, array $log = array())
		{
			$this->report = $job;
		}

		public function hasView(): bool
		{
			return (bool)$this->report->getView();
		}

		/**
		 * Build the message.
		 *
		 * @return $this
		 */
		public function build()
		{
			$job = $this->report->current();
			$view = $this->report->getView();

			return $this->markdown($view, ['report' => $this->report, 'job' => $job]);
		}
	}
